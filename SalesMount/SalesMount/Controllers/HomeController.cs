﻿using SalesMount.Utilities;
using SalesMount.Utilities.Communication;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesMount.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
			//AddHtmlView("TestEmail", new ViewDataDictionary());
			return View();
        }

		private class FakeController : Controller
		{
			protected override void ExecuteCore() { }
		}
		public void AddHtmlView(string viewToRender, ViewDataDictionary viewData)
		{
			//var result = ViewEngines.Engines.FindView(controllerContext, viewToRender, null);
			string viewStr;

			using (StringWriter output = new StringWriter())
			{
				var routeData = new RouteData();
				routeData.Values.Add("controller", "Home");

				var fakeControllerContext = new ControllerContext(new HttpContextWrapper(new HttpContext(new HttpRequest(null, "http://localhost/", null), new HttpResponse(null))), routeData, new FakeController());
				var razorViewEngine = new RazorViewEngine();
				var razorViewResult = razorViewEngine.FindView(fakeControllerContext, viewToRender, "", false);

				var viewContext = new ViewContext(fakeControllerContext, razorViewResult.View, new ViewDataDictionary(), new TempDataDictionary(), output);

				razorViewResult.View.Render(viewContext, output);
				razorViewResult.ViewEngine.ReleaseView(fakeControllerContext, razorViewResult.View);
				viewStr = output.ToString();
			}
		}

	}
}