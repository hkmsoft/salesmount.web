﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SalesMount.Models.Db;
using SalesMount.Utilities;
using System.Data.Entity.Validation;
using System.Diagnostics;
using SalesMount.Models.Forms;
using SalesMount.Utilities.Communication;
using SalesMount.Models.BLL;
using SalesMount.Lang;
using SalesMount.Models;
using System.Text.RegularExpressions;

namespace SalesMount.Controllers
{
    public class UsersController : Controller
    {
        private SalesMountContext db = new SalesMountContext();
		private Regex mailRgx = new Regex(
				@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
				@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$");
		private Regex telRgx = new Regex(@"^[9|6|5][\d]{7}$");

        public ActionResult Signup()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Signup([Bind(Include = "LastName,FirstName,DisplayName,Email,Password,ConfirmPassword,Gender,BYear,BMonth,BDay")] RegistrationForm registration)
		{
			if (ModelState.IsValid)
			{
				try
				{
					UserActivationCode activationCode;
					UsersBLL usersBll = new UsersBLL(db);
					if (usersBll.IsExist(registration.Email))
					{
						ModelState.AddModelError("Email", GetText.T("Email was already used."));
					}
					else
					{
						User user = usersBll.Registration(registration, out activationCode);

						SendActiveCodeMail(user, activationCode.Code);
						return View("SignupSuccess", user);
					}
				}
				catch (DbEntityValidationException dbEx)
				{
					foreach (var validationErrors in dbEx.EntityValidationErrors)
					{
						foreach (var validationError in validationErrors.ValidationErrors)
						{
							Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
						}
					}
				}

			}

			return View(registration);
		}

		private void SendActiveCodeMail(User user, String activeCode)
		{
			if (telRgx.IsMatch(user.Email))
			{
				var sender = new SmsSender();
				String activeUrl = Url.Action("activation", "users", new { email = user.Email, activationCode = activeCode }, Request.Url.Scheme);
				String message = GetText.T("Your activation code: {0}\nOr click: {1}", activeCode, activeUrl);

				sender.Send("+852", user.Email, message);
			}
			else
			{
				var accountEmailInfo = SiteConfig.GetEmailInfo("account");
				MailSender mail = accountEmailInfo.GetMailSender();
				mail.Receivers.Add(user.Email);

				var viewData = new ViewDataDictionary();
				viewData.Add("FirstName", user.FirstName);
				viewData.Add("Email", user.Email);
				viewData.Add("ActivationCode", activeCode);
				viewData.Add("Title", "Account Activation");

				mail.AddHtmlView("ActivationMail", viewData, ControllerContext);
				mail.Title = "SalesMount Account Activation";
				mail.Send();
			}
		}

		public ActionResult ActivationMail()
		{
			User user = new User();
			user.FirstName = "Test";
			user.LastName = "Tester";
			user.Email = "dummysw2014@gmail.com";
			SendActiveCodeMail(user, "00000000");
			ViewData.Add("FirstName", "Tester");
			ViewData.Add("ActivationCode", "0000000");
			ViewData.Add("Email", "test@test.com");
			ViewData.Add("Title", "Account Activation");
			return View();
		}

		public ActionResult Activation()
		{
			ViewBag.IsSuccess = false;
			return View();
		}

		[HttpGet]
		public ActionResult Activation([Bind(Include = "Email,ActivationCode")] ActivationForm activationForm)
		{
			ViewBag.IsSuccess = false;
			if (ModelState.IsValid)
			{
				UsersBLL usersBll = new UsersBLL(db);
				ViewBag.IsSuccess = usersBll.Activate(activationForm.Email, activationForm.ActivationCode);
			}
			return View();
		}

		public ActionResult Login(String redirect)
		{
			if (redirect != null && redirect != "")
			{
				return View(new LoginForm()
				{
					RedirectTo = redirect
				});
			}
			else
			{
				return View();
			}
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public ActionResult Login([Bind(Include = "Email,Password,RedirectTo")] LoginForm loginForm)
		{
			if (ModelState.IsValid)
			{
				User user;
				UsersBLL usersBll = new UsersBLL(db);

				UserLoginTypes loginResult = usersBll.CheckForLogin(loginForm, out user);
				switch (loginResult)
				{
					case UserLoginTypes.Fail:
						ModelState.AddModelError("", GetText.T("The email or password you entered is incorrect."));
						break;
					case UserLoginTypes.Success:
						LoginedSession.SetCurrentUser(user);
						
						if (loginForm.RedirectTo != null)
						{
							String redirectTo = loginForm.RedirectTo.Trim();
							if (!redirectTo.Contains("://"))
							{
								return Redirect(redirectTo);
							}
						}
						return RedirectToAction("Index", "Home");
					case UserLoginTypes.TryTooMuch:
						// Does not be implemented
						break;
					default:
						break;
				}
			}
			return View(loginForm);
		}

		public ActionResult Logout()
		{
			LoginedSession.Current = null;
			return RedirectToAction("Index", "Home");
		}

		public ActionResult sms()
		{
			var sender = new SmsSender();
			sender.Send("+852", "92480152", "Activtion code: " + new RandomString(8).ToString());
			return View();
		}

		public ActionResult Orders(Guid id)
		{
			//UsersBLL usersBll = new UsersBLL(db);
			OrderBLL orderBll = new OrderBLL(db);
			List<Order> orders = orderBll.GetUserOrders(id);
			ViewBag.orders = orders;
			return View();
		}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
