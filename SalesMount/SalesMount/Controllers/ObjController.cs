﻿using SalesMount.Models.Db;
using SalesMount.Utilities;
using SalesMount.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesMount.Controllers
{
    public class ObjController : Controller
    {
		private SalesMountContext db = new SalesMountContext();
        public ActionResult Index(String id)
        {
			try
			{
				Guid tagGuid;
				if (id.Contains("-"))
				{
					if (!Guid.TryParse(id, out tagGuid))
					{
						return HttpNotFound();
					}
				}
				else
				{
					tagGuid = GuidHelper.ParseBase64(id);
				}
				ViewBag.TagGuid = tagGuid;
				var tags = db.Identifiers.Where(i => i.TagGuid == tagGuid);
				if (tags != null && tags.Count() == 1)
				{
					if (AppHelper.IsApp())
					{
						var tag = tags.First();
						return RedirectToAction("Index", "Restaurant", new { id = tag.Restaurant.ID, sourceId = tagGuid });
					}
					else
					{
						return View("IndexInstall");
					}
				}
				else
				{
					return View();
				}
			}
			catch (Exception)
			{
				return HttpNotFound();
			}
			
        }

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}