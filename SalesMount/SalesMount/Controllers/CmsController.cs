﻿using SalesMount.Filters;
using SalesMount.Models;
using SalesMount.Models.BLL;
using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using SalesMount.Models.Forms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace SalesMount.Controllers
{
	[UserAuthorize(Roles = UserRoleType.User)]
	public class CmsController : Controller
	{
		private SalesMountContext db = new SalesMountContext();

		public ActionResult Index()
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			var destails = restaurantsBll.FindLocalRestaurantDetailListByUserId(LoginedSession.Current.UserId);
			return View(destails);
		}

		public ActionResult Restaurant(int? id, int? deatailId)
		{
			var form = new RestaurantForm();
			if (id != null)
			{
				RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
				RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id.Value);
				RestaurantDetail detail;
				if (deatailId == null)
				{
					detail = new RestaurantDetail()
					{
						Restaurant = restaurantDetail.Restaurant,
						Language = (restaurantDetail.Language == "zh-HK") ? "en-US" : "zh-HK"
					};
				}
				else
				{
					detail = restaurantsBll.GetRestaurantDetailById(deatailId.Value);
				}
				form.FromRestaurantDetail(detail);
				ViewBag.RestaurantDetail = restaurantDetail;
				ViewBag.RestaurantId = id.Value;
			}
			ViewBag.IsCreate = true;
			return View(form);
		}

		[HttpPost]
		public ActionResult Restaurant([Bind(Include = "DetailID,RestaurantId,Language,Name,Description,Tel,Address,Image")] RestaurantForm restaurantForm, HttpPostedFileBase imageFile)
		{
			if (ModelState.IsValid)
			{
				UsersBLL userBll = new UsersBLL(db);
				User user = userBll.FindUser(LoginedSession.Current.UserId);
				RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);

				if (imageFile != null && imageFile.ContentLength > 0)
				{
					var extName = Path.GetExtension(imageFile.FileName);
					var image = Guid.NewGuid() + extName;
					var path = Path.Combine(Server.MapPath("~/Content/Uploads"), image);
					imageFile.SaveAs(path);
					restaurantForm.Image = image;
				}

				restaurantsBll.SaveDetail(restaurantForm, user);

				return RedirectToAction("RestaurantDetails", new { id = restaurantForm.RestaurantId });
			}

			return View(restaurantForm);
		}

		public ActionResult RestaurantMenu(int id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id);
			Dictionary<String, RestaurantDetail> details = restaurantsBll.FindRestaurantDetail(id);
			
			foreach (var deatail in details)
			{
				db.Entry(deatail.Value).Collection(m => m.Menus).Load();
				foreach (var menu in deatail.Value.Menus)
				{
					db.Entry(menu).Collection(m => m.RestaurantMenuItems).Load();
				}
			}

			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.Details = details;
			ViewBag.RestaurantId = id;
			return View();
		}

		public ActionResult RestaurantDetails(int id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id);
			Dictionary<String, RestaurantDetail> restaurantDetails = restaurantsBll.FindRestaurantDetail(id);

			ViewBag.RestaurantDetails = restaurantDetails;
			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = id;
			return View();
		}

		public ActionResult RestaurantMenuEdit(int id, int deatailId, int? menuId)
		{
			RestaurantMenuForm restaurantMenuForm;
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.GetRestaurantDetailById(deatailId);

			RestaurantMenu restaurantMenu;
			if (menuId == null)
			{
				restaurantMenu = new RestaurantMenu()
				{
					Detail = restaurantDetail
				};

				restaurantMenuForm = new RestaurantMenuForm();
				restaurantMenuForm.DetailID = restaurantDetail.ID;
				restaurantMenuForm.RestaurantId = id;
			}
			else
			{
				restaurantMenu = restaurantsBll.GetRestaurantMenuById(menuId.Value);
				restaurantMenuForm = RestaurantMenuForm.FromRestaurantMenu(restaurantMenu);
			}

			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = id;


			return View(restaurantMenuForm);
		}

		[HttpPost]
		public ActionResult RestaurantMenuEdit(RestaurantMenuForm restaurantMenuForm, HttpPostedFileBase imageFile)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(restaurantMenuForm.RestaurantId);
			if (ModelState.IsValid)
			{

				if (imageFile != null && imageFile.ContentLength > 0)
				{
					var extName = Path.GetExtension(imageFile.FileName);
					var image = Guid.NewGuid() + extName;
					var path = Path.Combine(Server.MapPath("~/Content/Uploads"), image);
					imageFile.SaveAs(path);
					restaurantMenuForm.Image = image;
				}

				restaurantsBll.SaveMenu(restaurantMenuForm);

				return RedirectToAction("RestaurantMenu", new { id = restaurantMenuForm.RestaurantId });
			}

			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = restaurantMenuForm.RestaurantId;

			return View(restaurantMenuForm);
		}

		public ActionResult RestaurantOrders(int id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id);
			OrderBLL orderBll = new OrderBLL(db);
			List<Order> orders = orderBll.GetRestaurantOrders(id);
			foreach (var item in orders)
			{
				db.Entry(item).Reference(i => i.SourceIdentifier);
			}
			ViewBag.Orders = orders;
			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = id;
			return View();
		}

		public ActionResult RestaurantTags(int id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id);
			List<Identifier> identifiers = restaurantsBll.FindRestaurantTags(id);

			ViewBag.Identifiers = identifiers;
			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = id;
			return View();
		}

		public ActionResult RestaurantStat(int id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			RestaurantDetail restaurantDetail = restaurantsBll.FindLocalRestaurantDetail(id);

			ViewBag.RestaurantDetail = restaurantDetail;
			ViewBag.RestaurantId = id;
			return View();
		}

		[HttpPost]
		public JsonResult SaveRestaurant(Restaurant restaurant)
		{
			return Json(new { isSuccess = true } );
		}

		public ActionResult AddTag(Guid id)
		{
			RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
			var destails = restaurantsBll.FindLocalRestaurantDetailListByUserId(LoginedSession.Current.UserId);
			IdentifierForm identifier = new IdentifierForm();
			identifier.TagGuid = id;
			ViewBag.Destails = destails;
			return View(identifier);
		}

		[HttpPost]
		public ActionResult AddTag(IdentifierForm identifier)
		{
			if (ModelState.IsValid)
			{
				RestaurantsBLL restaurantsBll = new RestaurantsBLL(db);
				restaurantsBll.AddTag(identifier);
				return RedirectToAction("RestaurantTags", new { @id = identifier.RestaurantId });
			}
			return View(identifier);
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
	}
}