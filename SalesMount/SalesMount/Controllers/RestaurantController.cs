﻿using SalesMount.Models.BLL;
using SalesMount.Models.Db;
using SalesMount.Models.Forms;
using SalesMount.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SalesMount.Controllers
{
    public class RestaurantController : Controller
    {
		private SalesMountContext db = new SalesMountContext();

        // GET: Restaurant
		public ActionResult Index(int? id, String sourceId)
        {
			if (id == null)
			{
				return View("DemoPage");
			}

			RestaurantsBLL restaurantsBLL = new RestaurantsBLL(db);
			RestaurantDetail deatail = restaurantsBLL.FindLocalRestaurantDetail(id.Value);
			db.Entry(deatail).Collection(m => m.Menus).Load();
			foreach (var menu in deatail.Menus)
			{
				db.Entry(menu).Collection(m => m.RestaurantMenuItems).Load();
			}
			ViewBag.RestaurantId = id.Value;
			ViewBag.Deatail = deatail;
			ViewBag.SourceId = sourceId;
			return View();
        }

		[HttpPost]
		public ActionResult Order(OrderForm order)
		{
			if (ModelState.IsValid)
			{
				var items = order.Items.Where(i => i.Quantity > 0);
				if (items.Count() > 0)
				{
					if (order.UserGuid == null)
					{
						order.UserGuid = new Guid();
					}
					OrderBLL orderBLL = new OrderBLL(db);
					try
					{
						orderBLL.CreateOrder(order);
						return View(items.ToList());
					}
					catch (Exception)
					{
					}
				}
			}
			return View();
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}