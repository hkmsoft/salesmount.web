﻿using SalesMount.Filters;
using SalesMount.Models.BLL;
using SalesMount.Models.Db;
using SalesMount.Models.Forms.Charts;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;

namespace SalesMount.Controllers
{
    public class StatApiController : ApiController
    {
		private SalesMountContext db = new SalesMountContext();

		private void SetLang(String langCode)
		{
			try
			{
				Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(langCode);
				
			}
			catch (Exception)
			{
			}
		}

		public ChartJsData<LineChartData> GetHourlyIncome(int id, String langCode)
		{
			SetLang(langCode);
			StatBLL statBll = new StatBLL(db);
			var income = statBll.GetHourlyIncome(id, -12);

			var lineChartData = new LineChartData();

			var result = new ChartJsData<LineChartData>();
			result.labels = new List<string>();
			result.datasets = new List<LineChartData>();
			result.datasets.Add(lineChartData);

			lineChartData.data = new List<double>();
			for (int i = -12; i <= 0; i++)
			{
				var time = DateTime.Now.AddHours(i);
				result.labels.Add(time.ToString("HH:00"));
				if (income.ContainsKey(time.Hour))
				{
					lineChartData.data.Add(income[time.Hour]);
				}
				else
				{
					lineChartData.data.Add(0);
				}
			}

			return result;
		}

		public ChartJsData<LineChartData> GetDailyIncome(int id, String langCode)
		{
			SetLang(langCode);
			StatBLL statBll = new StatBLL(db);
			var income = statBll.GetDailyIncome(id, -14);

			var lineChartData = new LineChartData();

			var result = new ChartJsData<LineChartData>();
			result.labels = new List<string>();
			result.datasets = new List<LineChartData>();
			result.datasets.Add(lineChartData);

			lineChartData.data = new List<double>();
			for (int i = -14; i <= 0; i++)
			{
				var time = DateTime.Now.AddDays(i);
				result.labels.Add(time.ToString("dd MMM", Thread.CurrentThread.CurrentUICulture));
				if (income.ContainsKey(time.Day))
				{
					lineChartData.data.Add(income[time.Day]);
				}
				else
				{
					lineChartData.data.Add(0);
				}
			}

			return result;
		}

		public ChartJsData<LineChartData> GetMonthlyIncome(int id, String langCode)
		{
			SetLang(langCode);
			StatBLL statBll = new StatBLL(db);
			var income = statBll.GetMonthlyIncome(id, -12);

			var lineChartData = new LineChartData();

			var result = new ChartJsData<LineChartData>();
			result.labels = new List<string>();
			result.datasets = new List<LineChartData>();
			result.datasets.Add(lineChartData);

			lineChartData.data = new List<double>();
			for (int i = -11; i <= 0; i++)
			{
				var time = DateTime.Now.AddMonths(i);
				result.labels.Add(time.ToString("MMM yyyy"));
				if (income.ContainsKey(time.Month))
				{
					lineChartData.data.Add(income[time.Month]);
				}
				else
				{
					lineChartData.data.Add(0);
				}
			}

			return result;
		}

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				db.Dispose();
			}
			base.Dispose(disposing);
		}
    }
}
