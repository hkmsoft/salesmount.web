﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using SalesMount.Filters;
using SalesMount.Utilities.Communication;

namespace SalesMount
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

			GlobalFilters.Filters.Add(new LocalizationAttribute());

			//Accpet Mail server certificate
			ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
			{
				String certHash = certificate.GetCertHashString();
				switch (certHash)
				{
					case "2E80AAFE77819479592D16E79771505C1C5BE719":
					case "60E372CBDB0479E099BCF34DC35EAE97170520B0":
						return true;
					default:
						return false;
				}
			};
        }
    }
}