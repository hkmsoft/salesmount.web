/**
 * @author HKM
 */
(function($){

	var listRule = /^([\d\w]+)\[([\w|last]*)\]$/;
	
	/**
	 *
	 * @param {Object} obj
	 * @param {string} name
	 * @param {Object} val
	 */
	function setSpacedVal(obj, name, val){
		var spName = name.split(".");
		var tmp = obj;
		
		var upperCount = spName.length - 1;
		
		for (var i = 0; i < upperCount; i++) {
		
			var lName = spName[i];
			var nameMach = lName.match(listRule);
			if (nameMach) {
				if (!tmp[nameMach[1]]) {
					tmp[nameMach[1]] = [];
				}
				
				if (nameMach[2] == "last") {
					var lastIndex = tmp[nameMach[1]].length-1;
					if (!tmp[nameMach[1]][lastIndex]) {
						tmp[nameMach[1]][lastIndex] = {};
					}
					tmp = tmp[nameMach[1]][lastIndex];
					
				} else {
					if (nameMach[2] == "" || nameMach[2] == null) {
						var nObj = {};
						tmp[nameMach[1]].push(nObj);
						tmp = nObj;
					} else {
						var index = parseInt(nameMach[2]);
						if (!tmp[nameMach[1]][index]) {
							tmp[nameMach[1]][index] = {};
						}
						tmp = tmp[nameMach[1]][index];
					}
				}
				
			} else {
				if (!tmp[lName]) {
					tmp[lName] = {};
				}
				tmp = tmp[lName];
			}
		}
		
		var lsName = spName[upperCount];
		var lsNameMach = lsName.match(listRule);
		if (lsNameMach) {
			if (!tmp[lsNameMach[1]]) {
				tmp[lsNameMach[1]] = [];
			}
			
			if (lsNameMach[2]!=null && lsNameMach[2] != "") {
				if (lsNameMach[2] == "last") {
					var lastIndex = tmp[lsNameMach[1]].length-1;
					tmp[lsNameMach[1]][lastIndex] = val;
				} else {
					var index = parseInt(lsNameMach[2]);
					
					tmp[lsNameMach[1]][index] = val;
				}
			} else {
				tmp[lsNameMach[1]].push(val);
			}
		} else {
			tmp[spName[upperCount]] = val;
		}
	}
	
	function arrayReform(obj){
		for(var i in obj){
			if(obj[i] instanceof Array){
				var nArr = [];
				for(var j in obj[i]){
					if(!isNaN(j) && obj[i][j] !== undefined){
						if(!(obj[i][j] instanceof Array) && typeof obj[i][j] === "object"){
							arrayReform(obj[i][j])
						}
						nArr.push(obj[i][j]);
					}
				}
				obj[i] = nArr;
			}else if(typeof obj[i] === "object"){
				arrayReform(obj[i])
			}
		}
	}
	
	$.fn.form2json = function(o){
		o || (o={});
		
		var input = $(":input", this);
		
		var obj = {};
		
		input.each(function(){
			var $this = $(this);
			
			var inputName = $this.attr("name");
			var inputType = $this.attr("type");
			if (inputName && inputName != "") {
				switch(inputType){
					case "checkbox":
						var inputVal = $this.val();
						if(inputVal && inputVal != null){
							if(this.checked)
								setSpacedVal(obj, $this.attr("name"), $this.val());
						}else{
							if(this.checked)
								setSpacedVal(obj, $this.attr("name"), "True");
							else
								setSpacedVal(obj, $this.attr("name"), "False");
						}
						
						break;
					default:
						setSpacedVal(obj, $this.attr("name"), $this.val());
						break;
				}
			}
		});
		
		if (o.reform !== false) {
			arrayReform(obj);
		}
		
		return obj;
	};
	
	$.fn.jsonForm = function(o){
		o || (o={});
		
		this.each(function(){
			if(this.tagName == "FORM"){
				var $this = $(this);
				$this.submit(function(event){
					event.preventDefault();
					$this.trigger("beforeJsonSubmit");
					$.ajax({
						url: this.action,
						dataType: "json",
						contentType: "application/json; charset=utf-8",
						type: this.method.toUpperCase(),
						data: JSON.stringify($this.form2json({"reform":o.reform})),
						success: function(data, textStatus, jqXHR) {
							$this.trigger("successJsonSubmit",[data, textStatus, jqXHR]);
						},
						error: function(jqXHR, textStatus, errorThrown) {
							$this.trigger("errorJsonSubmit",[jqXHR, textStatus, errorThrown]);
						},
						complete: function(jqXHR, textStatus){
							$this.trigger("afterJsonSubmit",[jqXHR, textStatus]);
						}
					});
					return false;
				});
			}
		});
		return this;
	};
	
})(jQuery);
