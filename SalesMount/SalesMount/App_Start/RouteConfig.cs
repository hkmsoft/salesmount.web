﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesMount
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				name: "ObjRoute",
				url: "obj/{id}",
				defaults: new { controller = "Obj", action = "Index" },
				constraints: new { id = @"^([\w\-_]{22,})$" }
			);

			routes.MapRoute(
				"LocalizationHiddenActionName", // Route name
				"{lang}/{controller}/{id}", // URL with parameters
				new { lang = UrlParameter.Optional, controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
				new { lang = @"^([\w]{2}-[\w]{2})|([\w]{2})$", id = @"^([\d]+)$" }
			);

			routes.MapRoute(
				"Localization", // Route name
				"{lang}/{controller}/{action}/{id}", // URL with parameters
				new { lang = UrlParameter.Optional, controller = "Home", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
				new { lang = @"^([\w]{2}-[\w]{2})|([\w]{2})$" }
			);

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
