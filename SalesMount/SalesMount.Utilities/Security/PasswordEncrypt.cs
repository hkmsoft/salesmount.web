﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities.Security
{
	public class PasswordEncrypt
	{
		public static string OneWayEncryptSalt(string rawText)
		{
			String salt = new RandomString(8).ToString();
			return salt + ":" + OneWayEncrypt(salt + rawText);
		}

		public static string OneWayEncrypt(string rawText)
		{
			byte[] s = Encoding.UTF8.GetBytes(rawText);
			SHA1CryptoServiceProvider sha1 = new SHA1CryptoServiceProvider();
			byte[] sha1Bits = sha1.ComputeHash(s);

			return Convert.ToBase64String(sha1Bits);
		}

		public static bool IsPasswordMatch(string passwordWithSalt, string rawPassword)
		{
			int saltStart = passwordWithSalt.IndexOf(':');
			if (saltStart != -1)
			{
				string hashedPw = passwordWithSalt.Substring(saltStart + 1);
				string salt = passwordWithSalt.Substring(0, saltStart);
				string hashed = OneWayEncrypt(salt + rawPassword);

				return hashedPw == hashed;
			}
			else
			{
				throw new ArgumentException("Invalid Password Salt.");
			}
		}
	}
}
