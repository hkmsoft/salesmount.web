﻿using SalesMount.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities
{
	public static class TimeHelper
	{
		public static string ToRelativeTime(this DateTime dateTime)
		{
			return GetRelativeTime(dateTime);
		}
		public static string GetRelativeTime(DateTime dateTime)
		{
			TimeSpan s = DateTime.Now.Subtract(dateTime);

			int dayDiff = (int)s.TotalDays;

			int secDiff = (int)s.TotalSeconds;

			if (dayDiff < 0 || dayDiff >= 31)
			{
				return dateTime.ToString(GetText.T("yyyy-M-d HH:mm:ss"));
			}

			if (dayDiff == 0)
			{
				if (secDiff < 60)
				{
					return GetText.T("just now");
				}

				if (secDiff < 120)
				{
					return GetText.T("1 minute ago");
				}

				if (secDiff < 3600)
				{
					return string.Format(
						GetText.T("{0} minutes ago"),
						Math.Floor((double)secDiff / 60));
				}

				if (secDiff < 7200)
				{
					return GetText.T("1 hour ago");
				}

				if (secDiff < 86400)
				{
					return string.Format(
						GetText.T("{0} hours ago"),
						Math.Floor((double)secDiff / 3600));
				}
			}

			if (dayDiff == 1)
			{
				return GetText.T("yesterday");
			}
			if (dayDiff < 7)
			{
				return string.Format(
					GetText.T("{0} days ago"),
					dayDiff);
			}

			return dateTime.ToString(GetText.T("yyyy-M-d HH:mm:ss"));
		}
	}
}
