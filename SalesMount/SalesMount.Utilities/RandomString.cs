﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities
{
	public class RandomString
	{
		private char[] charDict = { 
											'0','1','2','3','4','5','6','7','8','9',
											'A','B','C','D','E','F','G','H','I','J','K','L','M',
											'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
											'a','b','c','d','e','f','g','h','i','j','k','l','m',
											'n','o','p','q','r','s','t','u','v','w','x','y','z'
										};

		public int Length { get; private set; }
		private Random random;
		
		public RandomString(int length)
		{
			random = new Random();
			Length = length;
		}

		public String ToString()
		{
			StringBuilder temp = new StringBuilder();
			for (int i = 0; i < Length; i++)
			{
				temp.Append( charDict[random.Next(charDict.Length)] );
			}
			return temp.ToString();
		}

		public String ToString(int groupingInterval, Char separator)
		{
			StringBuilder temp = new StringBuilder();
			for (int i = 0; i < Length; i++)
			{
				if (i != 1 && i % groupingInterval == 1)
				{
					temp.Append(separator);
				}
				temp.Append( charDict[random.Next(charDict.Length)] );
			}
			return temp.ToString();
		}
	}
}
