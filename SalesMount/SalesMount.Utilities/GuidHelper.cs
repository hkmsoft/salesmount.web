﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities
{
	public static class GuidHelper
	{
		public static String ToBase64String(Guid guid)
		{
			return Convert.ToBase64String(guid.ToByteArray()).Replace("=", "").Replace('+', '-').Replace('/', '_');
		}

		public static Guid ParseBase64(String urlBase64)
		{
			String base64 = urlBase64.Replace("-", "+").Replace("_", "/");
			int paddingLen = base64.Length % 3;
			if (paddingLen > 0)
			{
				base64 = base64.PadRight(base64.Length + 3 - paddingLen, '=');
			}
			return new Guid(Convert.FromBase64String(base64));
		}
	}
}
