﻿using SalesMount.Utilities.Communication;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities
{
	public class SiteConfig
	{
		private static Dictionary<string, EmailInfo> _EmailsInfo;
		public static EmailInfo GetEmailInfo(string name)
		{
			if (_EmailsInfo == null)
			{
				_EmailsInfo = (Dictionary<string, EmailInfo>)ConfigurationManager.GetSection(@"emailSets/emails");
			}

			if (_EmailsInfo.ContainsKey(name))
			{
				return _EmailsInfo[name];
			}
			return null;

		}
	}
}
