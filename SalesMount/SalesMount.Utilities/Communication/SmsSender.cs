﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SalesMount.Utilities.Communication
{
	public class SmsSender
	{
		#region Variables
		public static readonly string smsUrl = SmsSettings.Url;
		public static readonly string smsUsername = SmsSettings.Username;
		public static readonly string smsPassword = SmsSettings.Password;
		public static readonly string smsFrom = SmsSettings.From;

		public static readonly string UserAgent = @"Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0";
		#endregion

		#region SendViaTrunk
		/// <summary>
		/// Send SMS Via Trunk using Get Method
		/// </summary>
		/// <param name="countryCode">Country Code, +852, +86</param>
		/// <param name="outgoingMobileNumber">Outgoing Mobile Number</param>
		/// <param name="text">Text Message</param>
		/// <remarks></remarks>
		public bool Send(string countryCode, string outgoingMobileNumber, string text)
		{
			string _username = HttpUtility.UrlEncode(smsUsername);
			string _password = HttpUtility.UrlEncode(smsPassword);
			string _from = HttpUtility.UrlEncode(smsFrom);
			string _to = HttpUtility.UrlEncode((countryCode + outgoingMobileNumber).ToString());
			string _text = HttpUtility.UrlEncode(text);

			string getData = "username=" + _username +
				"&password=" + _password +
				"&from=" + _from +
				"&to=" + _to +
				"&text=" + _text;

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(smsUrl + getData);
			request.CookieContainer = new CookieContainer();
			request.UserAgent = UserAgent;
			request.KeepAlive = false;
			request.Method = "GET";
			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			StreamReader streamReader = new StreamReader(response.GetResponseStream(), System.Text.Encoding.UTF8);
			String result = streamReader.ReadToEnd();
			return result != null;
		}
		#endregion
	}
}
