﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace SalesMount.Utilities.Communication
{
	public class EmailAccountsSectionHandler : IConfigurationSectionHandler
	{
		object IConfigurationSectionHandler.Create(object parent, object configContext, XmlNode section)
		{
			Dictionary<string, EmailInfo> emailList = new Dictionary<string, EmailInfo>();
			string smtpSever = null;
			int smtpPort = 0;
			foreach (XmlAttribute attrib in section.Attributes)
			{
				if (XmlNodeType.Attribute == attrib.NodeType)
				{
					switch (attrib.Name)
					{
						case "smtpSever":
							smtpSever = attrib.Value;
							break;
						case "smtpPort":
							smtpPort = Int32.Parse(attrib.Value);
							break;
					}
				}
			}

			if (smtpSever != null && smtpPort != 0)
			{
				int nodeCount = section.ChildNodes.Count;
				for (int i = 0; i < nodeCount; i++)
				{
					var node = section.ChildNodes[i];
					if (node.NodeType == XmlNodeType.Element && node.Name == "email")
					{
						string id = null, password = null, username = null;

						foreach (XmlAttribute attrib in node.Attributes)
						{
							if (XmlNodeType.Attribute == attrib.NodeType)
							{
								switch (attrib.Name)
								{
									case "id":
										id = attrib.Value;
										break;
									case "username":
										username = attrib.Value;
										break;
									case "password":
										password = attrib.Value;
										break;
								}
							}
						}

						if (!String.IsNullOrWhiteSpace(id) && !String.IsNullOrWhiteSpace(username)
							&& !String.IsNullOrWhiteSpace(password))
						{
							if (!emailList.ContainsKey(id))
							{
								emailList.Add(id, new EmailInfo(username, password, smtpSever, smtpPort));
							}
							else
							{
								throw new Exception("Config: Email Id must be unique.");
							}
						}
						else
						{
							throw new Exception("Config: Email Id, username, password cannot be empty.");
						}
					}
				}
			}
			else
			{
				throw new Exception("Config: smtpSever and smtpPort must be set.");
			}

			return emailList;
		}
	}
}
