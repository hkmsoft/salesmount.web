﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesMount.Utilities.Communication
{
	public class MailSender
	{
		private class FakeController : Controller
		{
			protected override void ExecuteCore() { }
		}
		//http://www.systemnetmail.com/faq/4.4.aspx
		private EmailInfo Sender;
		public List<string> Receivers { get; private set; }

		private MailMessage mail;

		public string Title
		{
			get { return mail.Subject; }
			set { mail.Subject = value; }
		}

		public string Subject
		{
			get { return mail.Subject; }
			set { mail.Subject = value; }
		}

		public MailSender(EmailInfo sender)
			: this(sender, null)
		{ }

		public MailSender(EmailInfo sender, params string[] receivers)
		{
			this.Sender = sender;
			if (receivers == null)
			{
				this.Receivers = new List<string>();
			}
			else
			{
				this.Receivers = new List<string>(receivers);
			}

			this.mail = new MailMessage();
		}

		/// <summary>
		/// Add HTML View from MVC's view
		/// </summary>
		/// <param name="viewToRender"></param>
		/// <param name="viewData"></param>
		/// <param name="controllerContext"></param>
		public void AddHtmlView(string viewToRender, ViewDataDictionary viewData, ControllerContext controllerContext)
		{
			//var result = ViewEngines.Engines.FindView(controllerContext, viewToRender, null);
			string viewStr;

			using (StringWriter output = new StringWriter())
			{
				var razorViewEngine = new RazorViewEngine();
				var razorViewResult = razorViewEngine.FindView(controllerContext, viewToRender, "", false);
				var viewContext = new ViewContext(controllerContext, razorViewResult.View, viewData, new TempDataDictionary(), output);
				
				razorViewResult.View.Render(viewContext, output);
				razorViewResult.ViewEngine.ReleaseView(controllerContext, razorViewResult.View);
				viewStr = output.ToString();
			}

			AddHtmlView(viewStr);
		}

		private static Regex resourceRule = new Regex(@"<!--resource[ 0-9a-zA-Z'""=\.\-\/~]{0,}-->", RegexOptions.IgnoreCase);
		private static Regex nameRule = new Regex(@"cid=""(?<value>[0-9a-zA-Z'\.\-\/~]{0,})""", RegexOptions.IgnoreCase);
		private static Regex srcRule = new Regex(@"src=""(?<value>[0-9a-zA-Z'\.\-\/~]{0,})""", RegexOptions.IgnoreCase);

		public void AddHtmlView(string html)
		{
			string hStr = resourceRule.Replace(html, "");
			AlternateView htmlView = AlternateView.CreateAlternateViewFromString(hStr, null, "text/html");

			MatchCollection resources = resourceRule.Matches(html);

			foreach (Match resource in resources)
			{
				Match nameMatch = nameRule.Match(resource.Value);
				string cid = (nameMatch.Success) ? nameMatch.Groups["value"].Value : null;

				if (cid != null)
				{
					Match srcMatch = srcRule.Match(resource.Value);
					string src = (srcMatch.Success) ? srcMatch.Groups["value"].Value : null;

					if (src != null)
					{
						LinkedResource logo = new LinkedResource(System.Web.HttpContext.Current.Server.MapPath(src));
						logo.ContentId = cid;
						htmlView.LinkedResources.Add(logo);
					}
				}
			}

			this.mail.AlternateViews.Add(htmlView);
		}

		public void AddTextView(string viewStr)
		{
			AlternateView htmlView = AlternateView.CreateAlternateViewFromString(viewStr, null, "text/plain");
			this.mail.AlternateViews.Add(htmlView);
		}

		public List<string> Send()
		{
			SmtpClient smtp = new SmtpClient();
			smtp.Host = this.Sender.SmtpSever;
			smtp.Port = this.Sender.SmtpPort;
			smtp.EnableSsl = true;
			smtp.UseDefaultCredentials = false;

			smtp.Credentials = new NetworkCredential(Sender.UserName, Sender.Password);

			mail.From = new MailAddress(Sender.UserName);

			List<string> failures = new List<string>();
			foreach (string receiver in this.Receivers)
			{
				mail.To.Clear();
				mail.To.Add(new MailAddress(receiver));
				try
				{
					smtp.Send(mail);
				}
				catch (Exception ex)
				{
					failures.Add(ex.Message);
				}
			}
			return failures;

		}

		public List<string> Send(string title, string html)
		{
			Title = title;
			AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, null, "text/html");

			return Send();
		}

		public List<string> Send(string title, string html, params string[] attachment)
		{
			Title = title;
			AlternateView htmlView = AlternateView.CreateAlternateViewFromString(html, null, "text/html");
			AddAttachment(attachment);

			return Send();
		}

		public void AddAttachment(params string[] files)
		{
			foreach (var file in files)
			{
				if (!string.IsNullOrWhiteSpace(file))
				{
					var filePath = System.Web.HttpContext.Current.Server.MapPath(file);
					if (File.Exists(filePath))
					{
						mail.Attachments.Add(new Attachment(filePath));
					}
				}
			}
		}
	}
}
