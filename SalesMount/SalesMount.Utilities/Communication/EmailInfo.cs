﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities.Communication
{
	public class EmailInfo
	{
		public string UserName { get; internal set; }
		internal string Password { get; set; }
		public string SmtpSever { get; internal set; }
		public int SmtpPort { get; internal set; }

		public EmailInfo(string username, string password, string smtpSever, int smtpPort)
		{
			this.UserName = username;
			this.Password = password;
			this.SmtpSever = smtpSever;
			this.SmtpPort = smtpPort;
		}

		public MailSender GetMailSender()
		{
			return new MailSender(this);
		}

		public MailSender GetMailSender(string receiver)
		{
			return new MailSender(this, receiver);
		}

		public MailSender GetMailSender(string[] receivers)
		{
			return new MailSender(this, receivers);
		}
	}
}
