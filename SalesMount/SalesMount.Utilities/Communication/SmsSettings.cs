﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Utilities.Communication
{
	public static class SmsSettings
	{
		public static string Url
		{
			get
			{
				return Setting<string>("Sms:Url");
			}
		}

		public static string Username
		{
			get
			{
				return Setting<string>("Sms:Username");
			}
		}

		public static string Password
		{
			get
			{
				return Setting<string>("Sms:Password");
			}
		}

		public static string From
		{
			get
			{
				return Setting<string>("Sms:From");
			}
		}

		private static T Setting<T>(string name)
		{
			string value = ConfigurationManager.AppSettings[name];

			if (value == null)
			{
				throw new Exception(String.Format("Could not find setting '{0}',", name));
			}

			return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
		}
	}
}
