﻿using SalesMount.Lang;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SalesMount.Filters
{
	public class LocalizationAttribute : ActionFilterAttribute
	{
		public override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			LangCode langCode = LangCode.GetInstance();
			if (filterContext.RouteData.Values["lang"] != null &&
					 !string.IsNullOrWhiteSpace(filterContext.RouteData.Values["lang"].ToString()))
			{
				var lang = filterContext.RouteData.Values["lang"].ToString();
				lang = langCode.ToSupportedLangCode(lang);

				try
				{
					Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(lang);
				}
				catch (Exception)
				{
					Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(LangCode.DefaultLangCode);
				}
			}
			else
			{
				var cookie = filterContext.HttpContext.Request.Cookies["CurrentUICulture"];
				var langHeader = string.Empty;

				try
				{
					if (cookie != null)
					{
						langHeader = langCode.ToSupportedLangCode(cookie.Value);
						Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(langHeader);
					}
					else
					{
						langHeader = langCode.ToSupportedLangCode(filterContext.HttpContext.Request.UserLanguages[0]);
						Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(langHeader);
					}
				}
				catch (Exception)
				{
					langHeader = LangCode.DefaultLangCode;
					Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(LangCode.DefaultLangCode);
				}

				filterContext.RouteData.Values["lang"] = langHeader;
			}
			HttpCookie _cookie = new HttpCookie("CurrentUICulture", Thread.CurrentThread.CurrentUICulture.Name);
			_cookie.Expires = DateTime.UtcNow.AddYears(1);
			filterContext.HttpContext.Response.SetCookie(_cookie);


			base.OnActionExecuting(filterContext);
		}
	}
}
