﻿using SalesMount.Models;
using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesMount.Filters
{
	public class UserAuthorizeAttribute : AuthorizeAttribute
	{
		// the "new" must be used here because we are hiding
		// the Roles property on the underlying class
		public new UserRoleType Roles;

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (httpContext == null)
				throw new ArgumentNullException("httpContext");

			UserRoleType role;
			LoginedSession loginSession = LoginedSession.Current;
			if (loginSession == null)
			{
				role = UserRoleType.Vister;
			}
			else
			{
				role = LoginedSession.Current.Role;
			}


			int roleComp = ((int)role) & ((int)Roles);
			if (roleComp != 0)
			{
				return true;
			}
			else if (role == Roles)
			{
				return true;
			}

			return false;
		}

		public override void OnAuthorization(AuthorizationContext filterContext)
		{
			if (filterContext == null)
				throw new ArgumentNullException("filterContext");


			if (AuthorizeCore(filterContext.HttpContext))
			{
				SetCachePolicy(filterContext);
			}
			else
			{
				HandleUnauthorizedRequest(filterContext);
			}
		}


		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			RedirectToRouteResult result = new RedirectToRouteResult("Localization",
					new RouteValueDictionary(new
					{
						controller = "Users",
						action = "Login",
						redirect = filterContext.HttpContext.Request.RawUrl
					}));

			filterContext.Result = result;
		}




		protected virtual void SetCachePolicy(AuthorizationContext filterContext)
		{
			// ** IMPORTANT **
			// Since we're performing authorization at the action level, the authorization code runs
			// after the output caching module. In the worst case this could allow an authorized user
			// to cause the page to be cached, then an unauthorized user would later be served the
			// cached page. We work around this by telling proxies not to cache the sensitive page,
			// then we hook our custom authorization code into the caching mechanism so that we have
			// the final say on whether a page should be served from the cache.
			HttpCachePolicyBase cachePolicy = filterContext.HttpContext.Response.Cache;
			cachePolicy.SetProxyMaxAge(new TimeSpan(0));
			cachePolicy.AddValidationCallback(CacheValidateHandler, null /* data */);
		}

		protected virtual void CacheValidateHandler(HttpContext context, object data, ref HttpValidationStatus validationStatus)
		{
			validationStatus = OnCacheAuthorization(new HttpContextWrapper(context));
		}
	}
}
