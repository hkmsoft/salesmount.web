﻿using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using SalesMount.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.BLL
{
	public class OrderBLL : BLLBase<Order>
	{
		public OrderBLL(SalesMountContext DbContext)
			: base(DbContext)
		{
			DbSet = DbContext.Orders;
		}

		public void CreateOrder(OrderForm orderForm)
		{
			Identifier sourceIdentifier = DbContext.Identifiers.First(i => i.TagGuid == orderForm.SourceId);
			if (orderForm.UserGuid != null && sourceIdentifier != null)
			{
				foreach (var item in orderForm.Items.Where(i => i.Quantity > 0))
				{
					var order = new Order()
					{
						UserGuid = orderForm.UserGuid.Value,
						SourceIdentifier = sourceIdentifier,
						RestaurantId = orderForm.RestaurantId,
						RestaurantMenuItemId = item.RestaurantMenuItemId,
						Name = item.Name,
						Price = item.Price,
						Quantity = item.Quantity,
						OrderTime = DateTime.Now,
						Status = OrderStatusType.Padding
					};
					Insert(order);
				}
				SaveChange();
			}
			else if (orderForm.UserGuid == null)
			{
				throw new NullReferenceException("User Guid is Null.");
			}
			else
			{
				throw new NullReferenceException("Identifier is Null.");
			}
		}

		public List<Order> GetUserOrders(Guid userGuid)
		{
			var orders = DbSet.Where(i => i.UserGuid == userGuid).OrderByDescending(o => o.OrderTime);
			return orders.ToList();
		}

		public List<Order> GetRestaurantOrders(int restaurantId)
		{
			var orders = DbSet.Where(i => i.RestaurantId == restaurantId).OrderByDescending(o => o.OrderTime);
			return orders.ToList();
		}
	}
}
