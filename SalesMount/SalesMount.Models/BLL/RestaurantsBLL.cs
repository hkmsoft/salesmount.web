﻿using SalesMount.Models.Db;
using SalesMount.Models.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Threading;

namespace SalesMount.Models.BLL
{
	public class RestaurantsBLL : BLLBase<Restaurant>
	{
		public RestaurantsBLL(SalesMountContext DbContext)
			: base(DbContext)
		{
			DbSet = DbContext.Restaurants;
		}

		public void SaveDetail(RestaurantForm form, User user)
		{
			using (var dbTransaction = DbContext.Database.BeginTransaction())
			{
				Restaurant restaurant;
				RestaurantDetail detail;
				if (form.DetailID == 0)
				{
					detail = new RestaurantDetail();
					form.ToRestaurantDetail(detail);
					if (form.RestaurantId == 0)
					{
						restaurant = new Restaurant();
						Insert(restaurant);

						UserRestaurant userRestaurant = new UserRestaurant();
						userRestaurant.User = user;
						userRestaurant.Restaurant = restaurant;
						userRestaurant.Permission = Db.Types.UserPermissionType.Owner;
						DbContext.UserRestaurant.Add(userRestaurant);
					}
					else
					{
						restaurant = DbSet.Where(i => i.ID == form.RestaurantId).First();
					}
					detail.Restaurant = restaurant;
				}
				else
				{
					detail = DbContext.RestaurantDetails.Where(i => i.ID == form.DetailID).First();
					form.ToRestaurantDetail(detail);
					restaurant = detail.Restaurant;
				}

				if (detail.ID == 0)
				{
					DbContext.RestaurantDetails.Add(detail);
				}
				else
				{
					Update(detail as Object);
				}
				SaveChange();
				dbTransaction.Commit();

				form.RestaurantId = restaurant.ID;
			}
			
		}

		public List<Restaurant> StaffOf(User user)
		{
			return DbContext.UserRestaurant.Where(i => i.User.ID == user.ID).Select(i => i.Restaurant).ToList();
		}

		public List<Restaurant> StaffOf(int userId)
		{
			return DbContext.UserRestaurant.Where(i => i.User.ID == userId).Select(i => i.Restaurant).ToList();
		}

		public Dictionary<String, RestaurantDetail> FindRestaurantDetail(int restaurantId)
		{
			return DbContext.RestaurantDetails.Where(i => i.Restaurant.ID == restaurantId).ToDictionary(i => i.Language);
		}

		public RestaurantDetail GetRestaurantDetailById(int restaurantDetailId)
		{
			return DbContext.RestaurantDetails.Find(restaurantDetailId);
		}

		public RestaurantMenu GetRestaurantMenuById(int restaurantMenuId)
		{
			return DbContext.RestaurantMenus.Find(restaurantMenuId);
		}

		public RestaurantDetail FindLocalRestaurantDetail(int restaurantDetailId)
		{
			Dictionary<String, RestaurantDetail> details = DbContext.RestaurantDetails.Where(i => i.Restaurant.ID == restaurantDetailId).ToDictionary(i => i.Language);
			String currLang = Thread.CurrentThread.CurrentUICulture.Name;
			if (details.ContainsKey(currLang))
			{
				return details[currLang];
			}
			else
			{
				return details.First().Value;
			}
		}

		public List<RestaurantDetail> FindLocalRestaurantDetailListByUserId(int userId)
		{
			String currLang = Thread.CurrentThread.CurrentUICulture.Name;
			var restaurants = DbContext.UserRestaurant.Where(i => i.User.ID == userId).Select(i => i.Restaurant);
			List<RestaurantDetail> result = new List<RestaurantDetail>(restaurants.Count());
			foreach (var restaurant in restaurants)
			{
				if (restaurant.Details.Count(r => r.Language == currLang) == 1)
				{
					result.Add(restaurant.Details.Single(r => r.Language == currLang));
				}
				else
				{
					result.Add(restaurant.Details.First());
				}
			}

			return result;
		}

		public Restaurant GetRestaurantById(int id)
		{
			return DbContext.Restaurants.Single(i => i.ID == id);
		}


		public List<Identifier> FindRestaurantTags(int id)
		{
			return DbContext.Identifiers.Where(i => i.Restaurant.ID == id).ToList();
		}

		public void SaveMenu(RestaurantMenuForm restaurantMenuForm)
		{
			RestaurantMenu menu;
			if (restaurantMenuForm.ID == 0)
			{
				menu = restaurantMenuForm.ToRestaurantMenu(new RestaurantMenu());
				menu.Detail = DbContext.RestaurantDetails.Find(restaurantMenuForm.DetailID);
				DbContext.RestaurantMenus.Add(menu);
				SaveChange();
			}
			else
			{
				menu = DbContext.RestaurantMenus.Single(m => m.ID == restaurantMenuForm.ID);
				restaurantMenuForm.ToRestaurantMenu(menu);
				Update(menu as Object);
			}

			if (restaurantMenuForm.Items != null && restaurantMenuForm.Items.Count() > 0)
			{
				foreach (var item in restaurantMenuForm.Items)
				{
					if (item.ID == 0)
					{
						if (!String.IsNullOrWhiteSpace(item.Name))
						{
							RestaurantMenuItem newItem = item.ToRestaurantMenuItem(new RestaurantMenuItem());
							newItem.RestaurantMenu = menu;
							newItem.Quota = -1;
							DbContext.RestaurantMenuItems.Add(newItem);
						}
					}
					else
					{
						RestaurantMenuItem currItem = DbContext.RestaurantMenuItems.Find(item.ID);
						if (!String.IsNullOrWhiteSpace(item.Name))
						{
							item.ToRestaurantMenuItem(currItem);
							Update(currItem as Object);
						}
						else
						{
							Delete(currItem as Object);
						}
					}
				}
			}

			SaveChange();
		}

		public void AddTag(IdentifierForm identifier)
		{
			Identifier id = new Identifier();
			id.TagGuid = identifier.TagGuid;
			id.Remark = identifier.Remark;
			id.Restaurant = GetRestaurantById(identifier.RestaurantId);

			DbContext.Identifiers.Add(id);
			SaveChange();
		}
	}
}
