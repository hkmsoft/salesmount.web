﻿using SalesMount.Models.Db;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.BLL
{
	public abstract class BLLBase<TModel>
		where TModel : class
	{
		protected SalesMountContext DbContext;
		protected DbSet<TModel> DbSet;
		public BLLBase(SalesMountContext DbContext)
		{
			this.DbContext = DbContext;
		}


		protected List<TModel> GetAllAsList()
		{
			List<TModel> list = DbSet.ToList();
			return list;
		}

		protected IQueryable<TModel> GetAllAsQueryable()
		{
			return DbSet.AsQueryable();
		}

		protected TModel Insert(TModel data)
		{
			return DbSet.Add(data);
		}

		protected void Update(Object data)
		{
			DbContext.Entry(data).State = EntityState.Modified;
		}

		protected void Delete(Object data)
		{
			DbContext.Entry(data).State = EntityState.Deleted;
		}

		protected IQueryable<TModel> GetByField(Expression<Func<TModel, bool>> expression)
		{
			return DbSet.Where(expression);
		}

		protected int SaveChange()
		{
			return DbContext.SaveChanges();
		}


		

		protected void _Dispose()
		{
			_Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void _Dispose(bool disposing)
		{
			if (disposing)
			{
				// Free other state (managed objects).
				DbContext.Dispose();
			}
			// Free your own state (unmanaged objects).
			// Set large fields to null.
		}

		~BLLBase()
		{
			_Dispose(true);
		}
	}
}
