﻿using SalesMount.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.BLL
{
	public class StatBLL : BLLBase<Order>
	{
		public StatBLL(SalesMountContext DbContext)
			: base(DbContext)
		{
			DbSet = DbContext.Orders;
		}

		public Dictionary<int, double> GetHourlyIncome(int restaurantId, int hours)
		{
			DateTime timeLimit = DateTime.Now.AddHours(hours);
			
			Dictionary<int, double> data = new Dictionary<int, double>();
			var rawData = DbContext.Orders.Where(o => o.OrderTime > timeLimit).GroupBy(x => x.OrderTime.Hour).ToList();
			var houlyData = rawData.Select(x => new KeyValuePair<int, Double>(x.Key, x.Sum(y => y.Price * y.Quantity))).ToList();

			foreach (var item in houlyData)
			{
				data.Add(item.Key, item.Value);
			}

			return data;
		}

		public Dictionary<int, double> GetDailyIncome(int restaurantId, int days)
		{
			DateTime timeLimit = DateTime.Now.AddDays(days);

			Dictionary<int, double> data = new Dictionary<int, double>();
			var rawData = DbContext.Orders.Where(o => o.OrderTime > timeLimit).GroupBy(x => x.OrderTime.Day).ToList();
			var houlyData = rawData.Select(x => new KeyValuePair<int, Double>(x.Key, x.Sum(y => y.Price * y.Quantity))).ToList();

			foreach (var item in houlyData)
			{
				data.Add(item.Key, item.Value);
			}

			return data;
		}


		public Dictionary<int, double> GetMonthlyIncome(int restaurantId, int months)
		{
			DateTime timeLimit = DateTime.Now.AddMonths(months);

			Dictionary<int, double> data = new Dictionary<int, double>();
			var rawData = DbContext.Orders.Where(o => o.OrderTime > timeLimit).GroupBy(x => x.OrderTime.Month).ToList();
			var houlyData = rawData.Select(x => new KeyValuePair<int, Double>(x.Key, x.Sum(y => y.Price * y.Quantity))).ToList();

			foreach (var item in houlyData)
			{
				data.Add(item.Key, item.Value);
			}

			return data;
		}

	}
}
