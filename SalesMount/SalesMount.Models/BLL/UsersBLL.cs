﻿using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using SalesMount.Models.Forms;
using SalesMount.Utilities;
using SalesMount.Utilities.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models.BLL
{
	public class UsersBLL : BLLBase<User>
	{
		public UsersBLL(SalesMountContext DbContext)
			:base(DbContext)
		{
			DbSet = DbContext.Users;
		}
		public void AddUser(User user)
		{
			Insert(user);
			SaveChange();
		}

		public User Registration(RegistrationForm registration, out UserActivationCode activationCode)
		{
			User user = registration.ToUser();
			user.Password = PasswordEncrypt.OneWayEncryptSalt(user.Password);
			user.Status = UserStatusType.Padding;
			
			activationCode = new UserActivationCode();
			activationCode.UserId = user.ID;
			activationCode.User = user;
			activationCode.Code = new RandomString(8).ToString();
			DbContext.UserActivationCodes.Add(activationCode);

			AddUser(user);
			return user;
		}

		public bool Activate(string email, string code)
		{
			string randCode = code.Replace("-", "");

			UserActivationCode activationCode = DbContext.UserActivationCodes.First(i => i.Code == code);

			if (activationCode != null)
			{
				User user = DbContext.Users.First(i => i.Email == email);
				if (user != null)
				{
					user.Status = UserStatusType.Active;
					SaveChange();
					return true;
				}
			}
			
			return false;
		}

		public UserLoginTypes CheckForLogin(LoginForm loginForm, out User user)
		{
			UserLoginTypes result = UserLoginTypes.Fail;
			user = null;
			var tempUser = GetByField(i => i.Email == loginForm.Email && i.Status == UserStatusType.Active);

			if (tempUser.Count() == 1)
			{
				User tUser = tempUser.First();

				if (PasswordEncrypt.IsPasswordMatch(tUser.Password, loginForm.Password))
				{
					user = tUser;
					result = UserLoginTypes.Success;
				}
			}

			return result;
		}

		public User FindUser(String email)
		{
			return GetByField(i => i.Email == email).First();
		}

		public User FindUser(int userId)
		{
			return GetByField(i => i.ID == userId).First();
		}

		public bool IsExist(String email)
		{
			var user = GetByField(i => i.Email == email);
			return user.Count() == 1;
		}

		public bool IsExist(int userId)
		{
			var user = GetByField(i => i.ID == userId);

			return user.Count() == 1;
		}

		public string CreateSession(User user)
		{
			String sessionKey = new RandomString(48).ToString();
			LoginSession loginSession = new LoginSession();
			loginSession.User = user;
			loginSession.ExpirationDate = DateTime.Today.AddMonths(6);
			DbContext.LoginSession.Add(loginSession);
			DbContext.SaveChanges();
			return sessionKey;
		}
	}
}
