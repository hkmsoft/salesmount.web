﻿using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class Order
	{
		[Required]
		public int ID { get; set; }

		public Guid UserGuid { get; set; }

		public virtual Identifier SourceIdentifier { get; set; }

		public int RestaurantId { get; set; }

		[Required]
		public int RestaurantMenuItemId { get; set; }

		[Required]
		public String Name { get; set; }

		[Required]
		public float Price { get; set; }

		[Required]
		public int Quantity { get; set; }

		public DateTime OrderTime { get; set; }

		[Required]
		public OrderStatusType Status { get; set; }
	}
}
