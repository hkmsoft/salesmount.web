﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class RestaurantDetail
	{
		[Key]
		[Required]
		public int ID { get; set; }

		[Index]
		[StringLength(5)]
		[Required]
		public String Language { get; set; }

		[Required]
		public String Name { get; set; }

		public String Description { get; set; }

		[StringLength(31)]
		public String Tel { get; set; }

		[StringLength(511)]
		public String Address { get; set; }

		[StringLength(255)]
		public String Image { get; set; }

		[Required]
		public virtual Restaurant Restaurant { get; set; }

        public virtual ICollection<RestaurantMenu> Menus { get; set; }
	}
}
