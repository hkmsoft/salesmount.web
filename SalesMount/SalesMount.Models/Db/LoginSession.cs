﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class LoginSession
	{
		[Required]
		public virtual User User { get; set; }
		[Key]
		[StringLength(64)]
		public String Key { get; set; }
		public DateTime ExpirationDate { get; set; }
	}
}
