﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class Restaurant
	{
		[Key]
		[Required]
		public int ID { get; set; }

		public virtual ICollection<RestaurantDetail> Details { get; set; }

        public virtual ICollection<UserRestaurant> Staffs { get; set; }

	}
}
