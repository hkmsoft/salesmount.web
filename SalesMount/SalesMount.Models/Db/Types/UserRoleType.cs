﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum UserRoleType
	{
		Vister = 0,
		User = 0x1,
		Admin = 0x8
	}
}
