﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum UserStatusType
	{
		Active = 1,
		Inactive = 2,
		Padding = 3
	}
}
