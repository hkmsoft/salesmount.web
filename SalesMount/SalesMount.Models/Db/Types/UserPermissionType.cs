﻿
using System;
namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum UserPermissionType
	{
		Owner = 0xff,
		Staff = 0x1
	}
}