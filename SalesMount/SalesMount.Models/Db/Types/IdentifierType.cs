﻿using System;
namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum IdentifierType
	{
		Restaurant = 1,
		RestaurantMenu = 2,
		RestaurantCurrentMenu = 3
	}
}