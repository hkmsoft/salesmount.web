﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum GenderType
	{
		NotSpecified = 0,
		Male = 1,
		Female = 2
	}
}
