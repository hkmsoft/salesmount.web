﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db.Types
{
	[Serializable]
	[Flags]
	public enum OrderStatusType
	{
		Padding = 1,
		Accept = 2,
		Reject = 3
	}
}
