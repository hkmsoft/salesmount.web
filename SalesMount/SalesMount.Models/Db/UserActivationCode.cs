﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class UserActivationCode
	{
		[Key]
		[Required,ForeignKey("User")]
		public int UserId { get; set; }

		[Required]
		[StringLength(16)]
		public String Code { get; set; }

		[Required]
		public virtual User User { get; set; }
	}
}
