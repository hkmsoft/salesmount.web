﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using SalesMount.Lang.Attributes;
using SalesMount.Models.Db.Types;

namespace SalesMount.Models.Db
{
	public class User
	{
		[Key]
		[Required]
		public int ID { get; set; }

		[Required]
		public Guid UserGuid { get; set; }

		[Required]
		[StringLength(60)]
		[LocalizedDisplayName("Last Name")]
		public String LastName { get; set; }

		[Required]
		[StringLength(60)]
		[LocalizedDisplayName("First Name")]
		public String FirstName { get; set; }

		[Required]
		[StringLength(100)]
		[LocalizedDisplayName("Display Name")]
		public String DisplayName { get; set; }

		public GenderType Gender { get; set; }

		public DateTime Birthdate { get; set; }

		[Required]
		[Index(IsUnique=true)]
		[StringLength(255)]
		[LocalizedDisplayName("Email/Phone")]
		public String Email { get; set; }

		[Required]
		[StringLength(64)]
		[LocalizedDisplayName("Password")]
		public String Password { get; set; }

		private UserStatusType _status = UserStatusType.Inactive;
		public UserStatusType Status
		{
			get { return _status; }
			set { _status = value; }
		}

		private DateTime _createDate = DateTime.Now;
		public DateTime CreateDate
		{
			get { return _createDate; }
			set { _createDate = value; }
		}

		public UserRoleType Role { get; set; }
		
	}
}
