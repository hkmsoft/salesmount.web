﻿using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class Identifier
	{
		[Key]
		public int ID { get; set; }

		[Required]
		[Index(IsUnique = true)]
		public Guid TagGuid { get; set; }

		public virtual Restaurant Restaurant { get; set; }

		public String Remark { get; set; }
	}
}
