﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class RestaurantMenuItem
	{
		[Key]
		public long ID { get; set; }

		[Required]
		public int Sequence { get; set; }

		[Required]
		[StringLength(255)]
		public String Name { get; set; }

		[Required]
		public float Price { get; set; }

		[Required]
		public int Quota { get; set; }

		[StringLength(255)]
		public String Image { get; set; }

		[Required]
		public virtual RestaurantMenu RestaurantMenu { get; set; }
	}
}
