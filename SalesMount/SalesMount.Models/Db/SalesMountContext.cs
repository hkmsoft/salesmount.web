﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
    public class SalesMountContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<RestaurantDetail> RestaurantDetails { get; set; }
        public DbSet<RestaurantMenu> RestaurantMenus { get; set; }
		public DbSet<RestaurantMenuItem> RestaurantMenuItems { get; set; }

        public DbSet<User> Users { get; set; }
		public DbSet<UserRestaurant> UserRestaurant { get; set; }
		public DbSet<LoginSession> LoginSession { get; set; }
		public DbSet<UserActivationCode> UserActivationCodes { get; set; }

		public DbSet<Identifier> Identifiers { get; set; }
		public DbSet<Order> Orders { get; set; }

        public SalesMountContext()
            : base("name=DefaultConnection")
        {

        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            /*modelBuilder.Entity<Restaurant>().HasMany(r => r.Staffs).WithMany(u => u.StaffOf).Map(m =>
            {
                m.MapLeftKey("RestaurantId");
                m.MapRightKey("UserId");
				m.ToTable("UserRestaurant");
            });*/
        }
    }
}
