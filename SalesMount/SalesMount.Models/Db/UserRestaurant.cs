﻿using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class UserRestaurant
    {
        [Key]
        public int ID { get; set; }

		[Index]
        public virtual User User { get; set; }

		[Index]
		public virtual Restaurant Restaurant { get; set; }

        [Required]
        public virtual UserPermissionType Permission { get; set; }
    }
}
