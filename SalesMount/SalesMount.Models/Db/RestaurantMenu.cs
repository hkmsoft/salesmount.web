﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Db
{
	public class RestaurantMenu
	{
		[Key]
		[Required]
		public int ID { get; set; }

		[Required]
		public int Sequence { get; set; }

		[Required]
		public int StartTime { get; set; }

		[Required]
		public int EndTime { get; set; }

		[StringLength(255)]
		public String Image { get; set; }

		[Required]
		[StringLength(255)]
		public String Title { get; set; }

		[StringLength(1023)]
		public String Description { get; set; }

		[Required]
		public virtual RestaurantDetail Detail { get; set; }

		public virtual ICollection<RestaurantMenuItem> RestaurantMenuItems { get; set; }

	}
}
