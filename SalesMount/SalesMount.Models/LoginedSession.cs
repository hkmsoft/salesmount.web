﻿using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SalesMount.Models
{
	public class LoginedSession
	{
		private const String sessionName = "Login";
		public int UserId { get; set; }
		public string Email { get; set; }
		public string DisplayName { get; set; }
		public UserRoleType Role { get; set; }

		public static void SetCurrentUser(User user)
		{
			LoginedSession loginSession = new LoginedSession()
			{
				UserId = user.ID,
				Email = user.Email,
				DisplayName = user.DisplayName,
				Role = user.Role
			};

			Current = loginSession;
		}

		public static LoginedSession Current
		{
			get
			{
				if (HttpContext.Current.Session[sessionName] != null)
				{
					return HttpContext.Current.Session[sessionName] as LoginedSession;
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (value == null)
				{
					HttpContext.Current.Session.Remove(sessionName);
				}
				else
				{
					HttpContext.Current.Session[sessionName] = value;
				}
			}
		}
	}
}
