namespace SalesMount.Models.Migrations
{
	using SalesMount.Models.Db;
	using SalesMount.Models.Db.Types;
	using System;
	using System.Collections.Generic;
	using System.Data.Entity;
	using System.Data.Entity.Migrations;
	using System.Data.Entity.Validation;
	using System.Linq;
	using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<SalesMount.Models.Db.SalesMountContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(SalesMount.Models.Db.SalesMountContext context)
        {
			
			var boss = new User
			{
				ID = 1,
				UserGuid = Guid.Parse("11111111-1111-1111-1111-000000000001"),
				CreateDate = DateTime.Now,
				DisplayName = "Tester",
				Email = "boss@salesmount.com",
				FirstName = "Boss",
				LastName = "Lee",
				Birthdate = new DateTime(1985, 5, 5),
				Password = "9WucA83u:Fw8+GjOQjcyYqEPSzzt3GKy8+Lw=",
				Status = UserStatusType.Active,
				Role = UserRoleType.User
			};
			var tester = new User
			{
				ID = 2,
				UserGuid = Guid.Parse("11111111-1111-1111-1111-000000000002"),
				CreateDate = DateTime.Now,
				DisplayName = "Tester",
				Email = "test@salesmount.com",
				FirstName = "Tom",
				LastName = "Smith",
				Birthdate = new DateTime(1985, 5, 5),
				Password = "aelsTqvq:Ms4OXeq1k+jey5i7o1/Pz2+/64I=",
				Status = UserStatusType.Active,
				Role = UserRoleType.User
			};

			var cityRestaurant = new Restaurant()
			{
				ID = 1,
				Details = new List<RestaurantDetail>()
			};
			
			var cityNboss = new UserRestaurant()
			{
				ID = 1,
				Permission = UserPermissionType.Owner,
				Restaurant = cityRestaurant,
				User = boss
			};


			
			var cityDetails_zhHk = new RestaurantDetail()
			{
				ID = 1,
				Language = "zh-HK",
				Name = "�����\�U",
				Description = "�����\�U���z���Ѭ���",
				Restaurant = cityRestaurant,
				Tel = "41234123",
				Address = "���䫰�D 1 ��",
				Image = "e2295c97-a2c9-40df-a578-26b585bce45c.jpg",
				Menus = new List<RestaurantMenu>()
			};
			cityRestaurant.Details.Add(cityDetails_zhHk);

			var menuSelected_zhHk = new RestaurantMenu()
			{
				ID = 1,
				Title = "�C����",
				Sequence = 1,
				StartTime = 0,
				EndTime = 86400, // 24hrs
				Detail = cityDetails_zhHk,
				Image = "f8e3b4ce-2708-4c9d-ba33-d2deb7cbdbe6.jpg",
				RestaurantMenuItems = new List<RestaurantMenuItem>()
			};
			cityDetails_zhHk.Menus.Add(menuSelected_zhHk);

			var menuRice_zhHk = new RestaurantMenu()
			{
				ID = 2,
				Title = "����",
				Sequence = 1,
				StartTime = 0,
				EndTime = 86400, // 24hrs
				Detail = cityDetails_zhHk,
				Image = "d6ccf675-d9e5-444c-98ff-b3625a62a854.jpg",
				RestaurantMenuItems = new List<RestaurantMenuItem>()
			};
			cityDetails_zhHk.Menus.Add(menuRice_zhHk);

			var food1_zhHk = new RestaurantMenuItem()
			{
				ID = 1,
				Sequence = 1,
				Name = "�滷���v����",
				Price = 32,
				Quota = -1
			};
			menuSelected_zhHk.RestaurantMenuItems.Add(food1_zhHk);

			var food2_zhHk = new RestaurantMenuItem()
			{
				ID = 2,
				Sequence = 1,
				Name = "���̤��L�J����",
				Price = 33,
				Quota = -1
			};
			menuSelected_zhHk.RestaurantMenuItems.Add(food2_zhHk);

			var food3_zhHk = new RestaurantMenuItem()
			{
				ID = 3,
				Sequence = 1,
				Name = "���ڽ�����",
				Price = 33,
				Quota = -1
			};
			menuRice_zhHk.RestaurantMenuItems.Add(food3_zhHk);

			var food4_zhHk = new RestaurantMenuItem()
			{
				ID = 4,
				Sequence = 1,
				Name = "�ͪ�������",
				Price = 34,
				Quota = -1
			};
			menuRice_zhHk.RestaurantMenuItems.Add(food4_zhHk);

			var food5_zhHk = new RestaurantMenuItem()
			{
				ID = 5,
				Sequence = 1,
				Name = "�誣��",
				Price = 34,
				Quota = -1
			};
			menuRice_zhHk.RestaurantMenuItems.Add(food5_zhHk);



			var cityDetails_enUs = new RestaurantDetail()
			{
				ID = 2,
				Language = "en-US",
				Name = "City Restaurant",
				Description = "Delicious food for you.",
				Restaurant = cityRestaurant,
				Tel = "41234123",
				Address = "1st Hong Kong City, Hong Kong",
				Image = "e2295c97-a2c9-40df-a578-26b585bce45c.jpg",
				Menus = new List<RestaurantMenu>()
			};
			cityRestaurant.Details.Add(cityDetails_enUs);

			var menuSelected_enUs = new RestaurantMenu()
			{
				ID = 3,
				Title = "Daily Selection",
				Sequence = 1,
				StartTime = 0,
				EndTime = 86400, // 24hrs
				Detail = cityDetails_enUs,
				Image = "f8e3b4ce-2708-4c9d-ba33-d2deb7cbdbe6.jpg",
				RestaurantMenuItems = new List<RestaurantMenuItem>()
			};
			cityDetails_enUs.Menus.Add(menuSelected_enUs);

			var menuRice_enUs = new RestaurantMenu()
			{
				ID = 4,
				Title = "Fried Rice",
				Sequence = 1,
				StartTime = 0,
				EndTime = 86400, // 24hrs
				Detail = cityDetails_enUs,
				Image = "d6ccf675-d9e5-444c-98ff-b3625a62a854.jpg",
				RestaurantMenuItems = new List<RestaurantMenuItem>()
			};
			cityDetails_enUs.Menus.Add(menuRice_enUs);

			var food1_enUs = new RestaurantMenuItem()
			{
				ID = 6,
				Sequence = 1,
				Name = "Choy Sum & Beef Brisket with Fried Noodles",
				Price = 32,
				Quota = -1
			};
			menuSelected_enUs.RestaurantMenuItems.Add(food1_enUs);

			var food2_enUs = new RestaurantMenuItem()
			{
				ID = 7,
				Sequence = 1,
				Name = "Fried Eggs & Ham with Rice",
				Price = 33,
				Quota = -1
			};
			menuSelected_enUs.RestaurantMenuItems.Add(food2_enUs);

			var food3_enUs = new RestaurantMenuItem()
			{
				ID = 8,
				Sequence = 1,
				Name = "Pineapple Shrimp with Fried Rice",
				Price = 33,
				Quota = -1
			};
			menuRice_enUs.RestaurantMenuItems.Add(food3_enUs);

			var food4_enUs = new RestaurantMenuItem()
			{
				ID = 9,
				Sequence = 1,
				Name = "Fried Chicken with Rice",
				Price = 34,
				Quota = -1
			};
			menuRice_enUs.RestaurantMenuItems.Add(food4_enUs);

			var food5_enUs = new RestaurantMenuItem()
			{
				ID = 10,
				Sequence = 1,
				Name = "Western Fried Rice",
				Price = 34,
				Quota = -1
			};
			menuRice_enUs.RestaurantMenuItems.Add(food5_enUs);


			var tag1 = new Identifier
			{
				ID = 1,
				TagGuid = Guid.Parse("11111111-1111-1111-2222-000000000001"),
				Remark = "Table 1",
				Restaurant = cityRestaurant
			};

			context.Users.AddOrUpdate(tester, boss);
			context.Restaurants.AddOrUpdate(cityRestaurant);
			context.UserRestaurant.AddOrUpdate(cityNboss);

			context.RestaurantDetails.AddOrUpdate(cityDetails_zhHk);
			context.RestaurantMenus.AddOrUpdate(menuSelected_zhHk, menuRice_zhHk);
			context.RestaurantMenuItems.AddOrUpdate(food1_zhHk, food2_zhHk, food3_zhHk, food4_zhHk, food5_zhHk);
			SaveChanges(context, "zh-HK");

			context.RestaurantDetails.AddOrUpdate(cityDetails_enUs);
			context.RestaurantMenus.AddOrUpdate(menuSelected_enUs, menuRice_enUs);
			context.RestaurantMenuItems.AddOrUpdate(food1_enUs, food2_enUs, food3_enUs, food4_enUs, food5_enUs);

			context.Identifiers.AddOrUpdate(tag1);

			SaveChanges(context, "en-US");
			
			
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

		private void SaveChanges(DbContext context, String name = "")
		{
			try
			{
				context.SaveChanges();
			}
			catch (DbEntityValidationException ex)
			{
				StringBuilder sb = new StringBuilder();

				sb.AppendFormat("Name: {0}\n", name);
				foreach (var failure in ex.EntityValidationErrors)
				{
					sb.AppendFormat("{0} : {1} failed validation\n", failure.Entry.Entity.GetType(), failure.Entry.CurrentValues.GetValue<int>("ID"));

					foreach (var error in failure.ValidationErrors)
					{
						sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
						sb.AppendLine();
					}
				}

				throw new DbEntityValidationException(
					"Entity Validation Failed - errors follow:\n" +
					sb.ToString(), ex
				); // Add the original exception as the innerException
			}
		}
    }


}
