namespace SalesMount.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChangeUserRestaurant : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RestaurantDetails", "Image", c => c.String(maxLength: 255));
            AddColumn("dbo.Users", "Gender", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Birthdate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Birthdate");
            DropColumn("dbo.Users", "Gender");
            DropColumn("dbo.RestaurantDetails", "Image");
        }
    }
}
