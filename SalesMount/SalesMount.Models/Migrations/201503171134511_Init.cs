namespace SalesMount.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Identifiers",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TagGuid = c.Guid(nullable: false),
                        Remark = c.String(),
                        Restaurant_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Restaurants", t => t.Restaurant_ID)
                .Index(t => t.TagGuid, unique: true)
                .Index(t => t.Restaurant_ID);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.RestaurantDetails",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Language = c.String(nullable: false, maxLength: 5),
                        Name = c.String(nullable: false),
                        Description = c.String(),
                        Tel = c.String(maxLength: 31),
                        Address = c.String(maxLength: 511),
                        Restaurant_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Restaurants", t => t.Restaurant_ID, cascadeDelete: true)
                .Index(t => t.Language)
                .Index(t => t.Restaurant_ID);
            
            CreateTable(
                "dbo.RestaurantMenus",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        StartTime = c.Int(nullable: false),
                        EndTime = c.Int(nullable: false),
                        Image = c.String(maxLength: 255),
                        Title = c.String(nullable: false, maxLength: 255),
                        Description = c.String(maxLength: 1023),
                        Detail_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RestaurantDetails", t => t.Detail_ID, cascadeDelete: true)
                .Index(t => t.Detail_ID);
            
            CreateTable(
                "dbo.RestaurantMenuItems",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Sequence = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        Price = c.Single(nullable: false),
                        Quota = c.Int(nullable: false),
                        Image = c.String(maxLength: 255),
                        RestaurantMenu_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.RestaurantMenus", t => t.RestaurantMenu_ID, cascadeDelete: true)
                .Index(t => t.RestaurantMenu_ID);
            
            CreateTable(
                "dbo.UserRestaurants",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Permission = c.Int(nullable: false),
                        Restaurant_ID = c.Int(),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Restaurants", t => t.Restaurant_ID)
                .ForeignKey("dbo.Users", t => t.User_ID)
                .Index(t => t.Restaurant_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserGuid = c.Guid(nullable: false),
                        LastName = c.String(nullable: false, maxLength: 60),
                        FirstName = c.String(nullable: false, maxLength: 60),
                        DisplayName = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false, maxLength: 255),
                        Password = c.String(nullable: false, maxLength: 64),
                        Status = c.Int(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .Index(t => t.Email, unique: true);
            
            CreateTable(
                "dbo.LoginSessions",
                c => new
                    {
                        Key = c.String(nullable: false, maxLength: 64),
                        ExpirationDate = c.DateTime(nullable: false),
                        User_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Key)
                .ForeignKey("dbo.Users", t => t.User_ID, cascadeDelete: true)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UserGuid = c.Guid(nullable: false),
                        RestaurantMenuItemId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Price = c.Single(nullable: false),
                        Quantity = c.Int(nullable: false),
                        OrderTime = c.DateTime(nullable: false),
                        Status = c.Int(nullable: false),
                        SourceIdentifier_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Identifiers", t => t.SourceIdentifier_ID)
                .Index(t => t.SourceIdentifier_ID);
            
            CreateTable(
                "dbo.UserActivationCodes",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        Code = c.String(nullable: false, maxLength: 16),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserActivationCodes", "UserId", "dbo.Users");
            DropForeignKey("dbo.Orders", "SourceIdentifier_ID", "dbo.Identifiers");
            DropForeignKey("dbo.LoginSessions", "User_ID", "dbo.Users");
            DropForeignKey("dbo.Identifiers", "Restaurant_ID", "dbo.Restaurants");
            DropForeignKey("dbo.UserRestaurants", "User_ID", "dbo.Users");
            DropForeignKey("dbo.UserRestaurants", "Restaurant_ID", "dbo.Restaurants");
            DropForeignKey("dbo.RestaurantDetails", "Restaurant_ID", "dbo.Restaurants");
            DropForeignKey("dbo.RestaurantMenuItems", "RestaurantMenu_ID", "dbo.RestaurantMenus");
            DropForeignKey("dbo.RestaurantMenus", "Detail_ID", "dbo.RestaurantDetails");
            DropIndex("dbo.UserActivationCodes", new[] { "UserId" });
            DropIndex("dbo.Orders", new[] { "SourceIdentifier_ID" });
            DropIndex("dbo.LoginSessions", new[] { "User_ID" });
            DropIndex("dbo.Users", new[] { "Email" });
            DropIndex("dbo.UserRestaurants", new[] { "User_ID" });
            DropIndex("dbo.UserRestaurants", new[] { "Restaurant_ID" });
            DropIndex("dbo.RestaurantMenuItems", new[] { "RestaurantMenu_ID" });
            DropIndex("dbo.RestaurantMenus", new[] { "Detail_ID" });
            DropIndex("dbo.RestaurantDetails", new[] { "Restaurant_ID" });
            DropIndex("dbo.RestaurantDetails", new[] { "Language" });
            DropIndex("dbo.Identifiers", new[] { "Restaurant_ID" });
            DropIndex("dbo.Identifiers", new[] { "TagGuid" });
            DropTable("dbo.UserActivationCodes");
            DropTable("dbo.Orders");
            DropTable("dbo.LoginSessions");
            DropTable("dbo.Users");
            DropTable("dbo.UserRestaurants");
            DropTable("dbo.RestaurantMenuItems");
            DropTable("dbo.RestaurantMenus");
            DropTable("dbo.RestaurantDetails");
            DropTable("dbo.Restaurants");
            DropTable("dbo.Identifiers");
        }
    }
}
