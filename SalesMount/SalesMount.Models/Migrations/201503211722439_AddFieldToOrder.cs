namespace SalesMount.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldToOrder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Orders", "RestaurantId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Orders", "RestaurantId");
        }
    }
}
