﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models
{
	public class DataException : Exception
	{
		public ModelStateDictionary ModelState { get; private set; }
		public DataException(ModelStateDictionary modelState)
		{
			ModelState = modelState;
		}

		public void ReplaceModelState(ModelStateDictionary reModState)
		{
			reModState.Clear();
			reModState.Merge(ModelState);
		}
	}
}
