﻿using SalesMount.Lang.Attributes;
using SalesMount.Models.Db;
using SalesMount.Models.Db.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	[NotMapped]
	public class RegistrationForm
	{
		[Required]
		[StringLength(60)]
		[LocalizedDisplayName("Last Name")]
		public String LastName { get; set; }

		[Required]
		[StringLength(60)]
		[LocalizedDisplayName("First Name")]
		public String FirstName { get; set; }

		[Required]
		[StringLength(100)]
		[LocalizedDisplayName("Display Name")]
		public String DisplayName { get; set; }

		[LocalizedDisplayName("Birthday")]
		public int BYear { get; set; }
		public int BMonth { get; set; }
		public int BDay { get; set; }

		public String Gender { get; set; }

		private String _email;
		[Required]
		[StringLength(255)]
		[LocalizedDisplayName("Email/Phone")]
		public String Email {
			get { return _email; } 
			set {
				if (value == null)
				{
					_email = value;
				}
				else
				{
					_email = value.ToLower();
				}
			}
		}

		[Required]
		[StringLength(64)]
		[LocalizedDisplayName("Password")]
		public String Password { get; set; }

		[Required]
		[StringLength(64)]
		[LocalizedDisplayName("Confirm Password")]
		[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
		public String ConfirmPassword { get; set; }

		public Guid UserGuid { get; set; }

		public User ToUser()
		{
			User user = new User();
			user.DisplayName = DisplayName;
			user.Email = Email;
			user.FirstName = FirstName;
			user.LastName = LastName;
			user.Password = Password;
			if (UserGuid.Equals(Guid.Empty))
			{
				UserGuid = Guid.NewGuid();
			}
			user.UserGuid = UserGuid;
			user.Birthdate = new DateTime(BYear, BMonth, BDay);
			if (Gender == "M")
			{
				user.Gender = GenderType.Male;
			}
			else
			{
				user.Gender = GenderType.Female;
			}
			user.Role = UserRoleType.User;
			return user;
		}

	}
}
