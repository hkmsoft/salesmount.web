﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class OrderItemForm
	{
		[Required]
		public int RestaurantMenuItemId { get; set; }

		[Required]
		public String Name { get; set; }

		[Required]
		public float Price { get; set; }

		[Required]
		public int Quantity { get; set; }
	}
}
