﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models.Forms
{
	interface IForm
	{
		ModelStateDictionary Validate();
		void MergeValidate(ModelStateDictionary modelState);
		void MergeValidate(ModelStateDictionary modelState, bool isKeepData);
	}
}
