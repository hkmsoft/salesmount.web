﻿using SalesMount.Lang;
using SalesMount.Lang.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models.Forms
{
	public class LoginForm
	{
		[Required]
		[LocalizedDisplayName("Email")]
		public String Email { get; set; }

		[Required]
		[LocalizedDisplayName("Password")]
		public String Password { get; set; }
		public String RedirectTo { get; set; }
	}
}
