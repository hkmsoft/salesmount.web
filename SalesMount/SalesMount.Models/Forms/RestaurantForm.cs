﻿using SalesMount.Lang.Attributes;
using SalesMount.Models.Db;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class RestaurantForm
	{

		public int DetailID { get; set; }
		public int RestaurantId { get; set; }

		[Required]
		[StringLength(5)]
		[LocalizedDisplayName("Info Language")]
		public String Language { get; set; }

		[Required]
		[MinLength(5)]
		[LocalizedDisplayName("Restaurant Name")]
		public String Name { get; set; }

		[LocalizedDisplayName("Description")]
		public String Description { get; set; }

		[Required]
		[StringLength(31)]
		[LocalizedDisplayName("Telephone")]
		public String Tel { get; set; }

		[Required]
		[StringLength(511)]
		[LocalizedDisplayName("Address")]
		public String Address { get; set; }

		public String Image { get; set; }

		public RestaurantDetail ToRestaurantDetail(RestaurantDetail detail)
		{
			detail.Language = Language;
			detail.Name = Name;
			detail.Description = Description;
			detail.Tel = Tel;
			detail.Address = Address;
			detail.Image = Image;
			return detail;
		}

		public void FromRestaurantDetail(RestaurantDetail detail)
		{
			DetailID = detail.ID;
			RestaurantId = detail.Restaurant.ID;
			Language = detail.Language;
			Name = detail.Name;
			Description = detail.Description;
			Tel = detail.Tel;
			Address = detail.Address;
			Image = detail.Image;
		}
	}
}
