﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class OrderForm
	{
		[Required]
		public Guid SourceId { get; set; }

		[Required]
		public int RestaurantId { get; set; }

		public Guid? UserGuid { get; set; }

		public List<OrderItemForm> Items { get; set; }
	}
}
