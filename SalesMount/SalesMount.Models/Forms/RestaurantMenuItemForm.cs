﻿using SalesMount.Lang.Attributes;
using SalesMount.Models.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class RestaurantMenuItemForm
	{
		public long ID { get; set; }

		public int Sequence { get; set; }

		[LocalizedDisplayName("Cuisine Name")]
		public string Name { get; set; }

		[LocalizedDisplayName("Price")]
		public float Price { get; set; }

		public static RestaurantMenuItemForm FromRestaurantMenuItem(RestaurantMenuItem item)
		{
			var itemEdit = new RestaurantMenuItemForm();

			itemEdit.ID = item.ID;
			itemEdit.Sequence = item.Sequence;
			itemEdit.Name = item.Name;
			itemEdit.Price = item.Price;

			return itemEdit;
		}

		public RestaurantMenuItem ToRestaurantMenuItem(RestaurantMenuItem item)
		{
			if (item == null)
			{
				item = new RestaurantMenuItem();
			}

			item.ID = this.ID;
			item.Sequence = this.Sequence;
			item.Name = this.Name;
			item.Price = this.Price;

			return item;
		}
	}
}
