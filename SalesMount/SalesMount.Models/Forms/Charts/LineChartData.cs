﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms.Charts
{
	public class LineChartData
	{
		public String label { get; set; }
		public String strokeColor { get; set; }
		public String pointColor { get; set; }
		public String pointStrokeColor { get; set; }
		public String pointHighlightFill { get; set; }
		public String pointHighlightStroke { get; set; }
		public List<double> data { get; set; }
	}
}


