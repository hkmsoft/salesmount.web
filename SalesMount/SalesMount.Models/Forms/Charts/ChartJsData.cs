﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms.Charts
{
	public class ChartJsData<T>
	{
		public List<String> labels { get; set; }
		public List<T> datasets { get; set; }
	}
}
