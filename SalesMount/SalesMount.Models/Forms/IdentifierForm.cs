﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class IdentifierForm
	{
		[Required]
		public Guid TagGuid { get; set; }

		[Required]
		public int RestaurantId { get; set; }

		[Required]
		public String Remark { get; set; }
	}
}
