﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models.Forms
{
	public abstract class FormBase : IForm
	{
		private ModelStateDictionary modelState = null;
		public ModelStateDictionary ModelState
		{
			get
			{
				if (modelState == null)
					modelState = _Validate();

				return modelState;
			}
			protected set
			{
				modelState = value;
			}
		}

		public void MergeValidate(ModelStateDictionary modelState)
		{
			modelState.Clear();
			modelState.Merge(this.Validate());
		}

		public void MergeValidate(ModelStateDictionary modelState, bool isKeepData)
		{
			if (!isKeepData)
				modelState.Clear();
			
			modelState.Merge(this.Validate());
		}

		public void ClearModelState()
		{
			modelState = null;
		}

		protected abstract ModelStateDictionary _Validate();

		public ModelStateDictionary Validate()
		{
			modelState = _Validate();
			return modelState;
		}
	}
}
