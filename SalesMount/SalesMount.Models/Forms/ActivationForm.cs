﻿using SalesMount.Lang.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class ActivationForm
	{
		[Required]
		[LocalizedDisplayName("Email")]
		public string Email { get; set; }

		[Required]
		[LocalizedDisplayName("Activation Code")]
		public string ActivationCode { get; set; }
	}
}
