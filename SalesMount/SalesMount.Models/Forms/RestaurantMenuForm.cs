﻿using SalesMount.Lang.Attributes;
using SalesMount.Models.Db;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Models.Forms
{
	public class RestaurantMenuForm
	{
		public int ID { get; set; }

		[Required]
		public int Sequence { get; set; }
		[Required]
		public int RestaurantId { get; set; }
		[Required]
		public int DetailID { get; set; }
		[Required]
		public String Description { get; set; }
		[Required]
		[LocalizedDisplayName("Menu Name")]
		[MaxLength(255)]
		public string Title { get; set; }

		public string Image { get; set; }

		

		public List<RestaurantMenuItemForm> Items { get; set; }

		public static RestaurantMenuForm FromRestaurantMenu(RestaurantMenu menu)
		{
			var menuEdit = new RestaurantMenuForm();

			menuEdit.ID = menu.ID;
			menuEdit.RestaurantId = menu.Detail.Restaurant.ID;
			menuEdit.DetailID = menu.Detail.ID;
			menuEdit.Sequence = menu.Sequence;
			menuEdit.Title = menu.Title;
			menuEdit.Description = menu.Description;
			menuEdit.Image = menu.Image;

			menuEdit.Items = new List<RestaurantMenuItemForm>();
			if (menu.RestaurantMenuItems != null && menu.RestaurantMenuItems.Count() > 0)
			{
				foreach (var item in menu.RestaurantMenuItems)
				{
					menuEdit.Items.Add(RestaurantMenuItemForm.FromRestaurantMenuItem(item));
				}
			}

			return menuEdit;
		}

		public RestaurantMenu ToRestaurantMenu(RestaurantMenu menu)
		{
			menu.ID = this.ID;
			menu.Sequence = this.Sequence;
			menu.Title = this.Title;
			menu.Description = this.Description;
			menu.Image = this.Image;
			/*var editedItemList = new List<RestaurantMenuItem>();

			if (this.Items != null && this.Items.Count() > 0)
			{
				foreach (var item in this.Items)
				{
					if (item.ID != 0)
					{
						var dbMenuItem = menu.RestaurantMenuItems.Single(i => i.ID == item.ID);
						editedItemList.Add(item.ToRestaurantMenuItem(dbMenuItem));
					}
					else
					{
						editedItemList.Add(item.ToRestaurantMenuItem(null));
					}
				}
			}
			menu.RestaurantMenuItems = editedItemList;*/

			return menu;
		}
	}
}
