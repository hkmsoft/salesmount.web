﻿using SalesMount.Models.Forms;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.ModelBinding;

namespace SalesMount.Models
{
	public class Validation<TSource>
	{
		private TSource source { get; set; }
		private ModelStateDictionary errors;
		public ModelStateDictionary Errors { get { return errors; } protected set { errors = value; } }

		public Validation(TSource source)
		{
			this.source = source;
			this.Errors = new ModelStateDictionary();
		}

		public void RequiredField(Expression<Func<TSource, object>> expression, string errorMsg)
		{
			object value = expression.Compile()(this.source);

			if (value is String && String.IsNullOrWhiteSpace(value.ToString()))
			{
				AddError(expression, errorMsg);
			}
			else if (value == null)
			{
				AddError(expression, errorMsg);
			}
		}

		public void EmailField(Expression<Func<TSource, string>> expression, string errorMsg)
		{
			if (!RegexTest(@"^(([^<>()[\]\\.,;:\s@\""]+"
				+ @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
				+ @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
				+ @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
				+ @"[a-zA-Z]{2,}))$", expression))
			{
				AddError(expression, errorMsg);
			}
		}

		public void EqualsField(Expression<Func<TSource, object>> expression1, Expression<Func<TSource, object>> expression2, string errorMsg)
		{
			object input1 = expression1.Compile()(this.source);
			object input2 = expression2.Compile()(this.source);

			if (!input1.Equals(input2))
			{
				AddError(expression1, errorMsg);
				AddError(expression2, errorMsg);
			}
		}

		public void MinLengthField(Expression<Func<TSource, object>> expression, int minLength, string errorMsg)
		{
			object input = expression.Compile()(this.source);

			if (input is String && input.ToString().Length < minLength)
			{
				AddError(expression, errorMsg);
			}
			else if (input is IList)
			{
				IList typeInput = input as IList;
				if (typeInput.Count < minLength)
				{
					AddError(expression, errorMsg);
				}
			}
		}
		public void MaxLengthField(Expression<Func<TSource, object>> expression, int maxLength, string errorMsg)
		{
			object input = expression.Compile()(this.source);
			if (input is String && input.ToString().Length > maxLength)
			{
				AddError(expression, errorMsg);
			}
			else if (input is IList)
			{
				IList typeInput = input as IList;
				if (typeInput.Count > maxLength)
				{
					AddError(expression, errorMsg);
				}
			}
		}

		public void RangeField<T>(Expression<Func<TSource, T>> expression, int minLength, int maxLength, string errorMsg)
		{
			object input = expression.Compile()(this.source);
			int len;
			if (input is String)
			{
				len = input.ToString().Length;
			}
			else if (input is IList)
			{
				IList typeInput = input as IList;
				len = typeInput.Count;
			}
			else if (input is int)
			{
				len = (int)input;
			}
			else
			{
				throw new ArgumentException("Unsupport data type.");
			}

			if (len > maxLength || len < minLength)
			{
				AddError(expression, errorMsg);
			}

		}

		public void PhoneField(Expression<Func<TSource, string>> expression, string errorMsg)
		{
			if (!RegexTest(@"^(\([+]{0,1}[0-9]{1,4}\)[0-9]{6,14}|[+]{0,1}[0-9]{6,16})$", expression))
			{
				AddError(expression, errorMsg);
			}
		}

		public void ValidateForm(Expression<Func<TSource, FormBase>> expression, string errorMsg)
		{
			FormBase form = expression.Compile()(this.source);
			if (!form.ModelState.IsValid)
			{
				AddError(expression, errorMsg);
			}
		}

		public void ValidateForms(Expression<Func<TSource, Object>> expression, string errorMsg)
		{
			IList forms = expression.Compile()(this.source) as IList;

			if (forms == null)
			{
				throw new ArgumentException("Field type must be IList<FormBase>");
			}

			bool IsErr = false;
			foreach (Object form in forms)
			{
				FormBase formTyped = form as FormBase;
				if (formTyped == null)
				{
					throw new ArgumentException("Field type must be IList<FormBase>");
				}
				IsErr = IsErr || !formTyped.ModelState.IsValid;

			}
			if (IsErr)
			{
				AddError(expression, errorMsg);
			}

		}


		private bool RegexTest(string pattern, Expression<Func<TSource, string>> expression)
		{
			string input = expression.Compile()(this.source);
			Regex regex = new Regex(pattern);
			if (input == null)
			{
				return false;
			}
			else
			{
				return regex.IsMatch(input);
			}
		}

		public void AddErrorIf(Expression<Func<TSource, object>> expression, bool isAdd, string errorMsg)
		{
			if (isAdd)
			{
				AddError(expression, errorMsg);
			}
		}

		public void AddError<T>(Expression<Func<TSource, T>> expression, string errorMsg)
		{
			MemberExpression memberEx = null;
			if (expression.Body is MemberExpression)
			{
				memberEx = expression.Body as MemberExpression;
			}
			else if (expression.Body is UnaryExpression)
			{
				var unaryEx = expression.Body as UnaryExpression;
				memberEx = unaryEx.Operand as MemberExpression;
			}
			else
			{
				throw new ArgumentException("Body not a MemberExpression or UnaryExpression.");

			}

			string name = memberEx.Member.Name;
			this.Errors.AddModelError(name, errorMsg);
		}
	}
}
