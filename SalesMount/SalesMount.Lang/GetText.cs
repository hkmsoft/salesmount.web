﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Lang
{
	public class GetText
	{
		public static string T(string key)
		{
			string text;

			string hashKey = "H_" + StringHash.GetHash(key);

			try
			{
				text = Message.ResourceManager.GetString(hashKey);
			}
			catch (Exception)
			{
				text = hashKey;
			}

			if (text == null)
				text = key;

			return text;
		}

		public static string T(string key, params string[] replaces)
		{
			string text = T(key);
			int length = replaces.Length;

			for (int i = 0; i < length; i++)
			{
				text = text.Replace("{" + i + "}", replaces[i]);
			}
			return text;
		}

		public static string T(string key, Dictionary<string, string> replaces)
		{
			string text = T(key);

			foreach (var replace in replaces)
			{
				text = text.Replace("%" + replace.Key + "%", replace.Value);
			}
			return text;
		}
	}
}
