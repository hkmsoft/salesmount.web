﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Lang
{
	public class LangCode
	{
		public const string DefaultLangCode = "en-US";

		private Dictionary<string, string> altLangCodes;

		private Dictionary<string, HashSet<string>> supportedLangCodes;

		private LangCode()
		{
			supportedLangCodes = new Dictionary<string, HashSet<string>>();
			HashSet<string> enRegin = new HashSet<string>(new string[] { "en" });
			enRegin.Add("us");

			HashSet<string> zhRegin = new HashSet<string>(new string[] { "hk", "cn" });
			supportedLangCodes.Add("zh", zhRegin);

			altLangCodes = new Dictionary<string, string>();
			altLangCodes.Add("zh-tw", "zh-hk");
		}

		private static LangCode langCode;
		public static LangCode GetInstance()
		{
			if (langCode == null)
			{
				langCode = new LangCode();
			}
			return langCode;
		}

		public string ToSupportedLangCode(string code)
		{
			code = code.ToLower();
			string[] codeReg = code.Split('-');
			if (altLangCodes.ContainsKey(code))
			{
				return altLangCodes[code];
			}
			else if (codeReg.Length > 0)
			{
				if (supportedLangCodes.ContainsKey(codeReg[0]))
				{
					if (codeReg.Length > 1)
					{
						if (supportedLangCodes[codeReg[0]].Contains(codeReg[1]))
						{
							return code;
						}
						else
						{
							return supportedLangCodes[codeReg[0]].First();
						}
					}
				}
			}
			return DefaultLangCode;
		}
		
	}
}
