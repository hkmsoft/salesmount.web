msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2015-04-08 22:34+0800\n"
"PO-Revision-Date: 2015-04-08 22:37+0800\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: zh_HK\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.7.3\n"
"X-Poedit-Basepath: ../../\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: LocalizedDisplayName;T;RawT\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: SalesMount.Lang\n"
"X-Poedit-SearchPathExcluded-1: SalesMount.Models/Migrations\n"

#: SalesMount.Models/Db/User.cs:24 SalesMount.Models/Forms/RegistrationForm.cs:19 SalesMount/Views/Users/Signup.cshtml:39 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:26 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:26
msgid "Last Name"
msgstr "名"

#: SalesMount.Models/Db/User.cs:29 SalesMount.Models/Forms/RegistrationForm.cs:24 SalesMount/Views/Users/Signup.cshtml:47 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:34 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:34 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:34
msgid "First Name"
msgstr "姓"

#: SalesMount.Models/Db/User.cs:34 SalesMount.Models/Forms/RegistrationForm.cs:29 SalesMount/Views/Users/Signup.cshtml:55 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:42 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:42 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:42
msgid "Display Name"
msgstr "暱稱"

#: SalesMount.Models/Db/User.cs:44 SalesMount.Models/Forms/RegistrationForm.cs:42
msgid "Email/Phone"
msgstr "Email/電話"

#: SalesMount.Models/Db/User.cs:49 SalesMount.Models/Forms/LoginForm.cs:20 SalesMount.Models/Forms/RegistrationForm.cs:59 SalesMount/Views/Users/Login.cshtml:33 SalesMount/Views/Users/Signup.cshtml:89 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Login.cshtml:33 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:58 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Login.cshtml:33 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:58 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Login.cshtml:33 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:58
msgid "Password"
msgstr "密碼"

#: SalesMount.Models/Forms/ActivationForm.cs:14 SalesMount.Models/Forms/LoginForm.cs:16 SalesMount/Views/Users/Activation.cshtml:26 SalesMount/Views/Users/Activation.cshtml:33 SalesMount/Views/Users/Login.cshtml:26 SalesMount/Views/Users/Signup.cshtml:81 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:33 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Login.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:50 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:33 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Login.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:50 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:26
#: SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:33 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Login.cshtml:26 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:50
msgid "Email"
msgstr "Email"

#: SalesMount.Models/Forms/ActivationForm.cs:18
msgid "Activation Code"
msgstr "啟動代碼"

#: SalesMount.Models/Forms/RegistrationForm.cs:32
msgid "Birthday"
msgstr "生日"

#: SalesMount.Models/Forms/RegistrationForm.cs:64
msgid "Confirm Password"
msgstr "確認密碼"

#: SalesMount.Models/Forms/RestaurantForm.cs:20
msgid "Restaurant Language"
msgstr "餐廳語言"

#: SalesMount.Models/Forms/RestaurantForm.cs:25
msgid "Restaurant Name"
msgstr "餐廳名稱"

#: SalesMount.Models/Forms/RestaurantForm.cs:28
msgid "Introduction"
msgstr "介紹"

#: SalesMount.Models/Forms/RestaurantForm.cs:33
msgid "Telephone"
msgstr "電話"

#: SalesMount.Models/Forms/RestaurantForm.cs:38
msgid "Address"
msgstr "地址"

#: SalesMount.Utilities/TimeHelper.cs:26 SalesMount.Utilities/TimeHelper.cs:72
msgid "yyyy-M-d HH:mm:ss"
msgstr "yyyy-M-d HH:mm:ss"

#: SalesMount.Utilities/TimeHelper.cs:33
msgid "just now"
msgstr "剛才"

#: SalesMount.Utilities/TimeHelper.cs:38
msgid "1 minute ago"
msgstr "1分鐘之前"

#: SalesMount.Utilities/TimeHelper.cs:44
#, csharp-format
msgid "{0} minutes ago"
msgstr "{0}分鐘之前"

#: SalesMount.Utilities/TimeHelper.cs:50
msgid "1 hour ago"
msgstr "1小時之前"

#: SalesMount.Utilities/TimeHelper.cs:56
#, csharp-format
msgid "{0} hours ago"
msgstr "{0}小時之前"

#: SalesMount.Utilities/TimeHelper.cs:63
msgid "yesterday"
msgstr "昨天"

#: SalesMount.Utilities/TimeHelper.cs:68
#, csharp-format
msgid "{0} days ago"
msgstr "{0}日之前"

#: SalesMount/Controllers/UsersController.cs:47
msgid "Email was already used."
msgstr "email 已經被使用"

#: SalesMount/Controllers/UsersController.cs:79
#, csharp-format
msgid ""
"Your activation code: {0}\n"
"Or click: {1}"
msgstr ""
"您的激活碼: {0}\n"
"或者點擊: {1}"

#: SalesMount/Controllers/UsersController.cs:161
msgid "The email or password you entered is incorrect."
msgstr "你輸入的 email 或密碼不正確。"

#: SalesMount/Views/Cms/Index.cshtml:4 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/Index.cshtml:4 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/Index.cshtml:4 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/Index.cshtml:4
msgid "Welcome"
msgstr "歡迎來到"

#: SalesMount/Views/Cms/Index.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/Index.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/Index.cshtml:10 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/Index.cshtml:10
msgid "Create restaurant"
msgstr "新增餐廳"

#: SalesMount/Views/Cms/Index.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/Index.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/Index.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/Index.cshtml:12
msgid "My Restaurants"
msgstr "我的餐廳"

#: SalesMount/Views/Cms/Restaurant.cshtml:21
msgid "Restaurant"
msgstr "餐廳"

#: SalesMount/Views/Cms/Restaurant.cshtml:27 SalesMount/Views/Cms/RestaurantDetails.cshtml:12 SalesMount/Views/Cms/RestaurantMenu.cshtml:9 SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:8 SalesMount/Views/Cms/RestaurantOrders.cshtml:9 SalesMount/Views/Cms/RestaurantStat.cshtml:14 SalesMount/Views/Cms/RestaurantTags.cshtml:8 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantDetails.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenu.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:8 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:14 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantTags.cshtml:8 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantDetails.cshtml:12
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenu.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:8 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:14 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantTags.cshtml:8 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantDetails.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenu.cshtml:11 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:8 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:9 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:14 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantTags.cshtml:8
msgid "Info"
msgstr "介紹"

#: SalesMount/Views/Cms/Restaurant.cshtml:28 SalesMount/Views/Cms/RestaurantDetails.cshtml:13 SalesMount/Views/Cms/RestaurantMenu.cshtml:10 SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:9 SalesMount/Views/Cms/RestaurantOrders.cshtml:10 SalesMount/Views/Cms/RestaurantStat.cshtml:15 SalesMount/Views/Cms/RestaurantTags.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantDetails.cshtml:13 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenu.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantTags.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantDetails.cshtml:13
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenu.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantTags.cshtml:9 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantDetails.cshtml:13 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenu.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:9 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:10 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantTags.cshtml:9
msgid "Menus"
msgstr "菜單"

#: SalesMount/Views/Cms/Restaurant.cshtml:29 SalesMount/Views/Cms/RestaurantDetails.cshtml:14 SalesMount/Views/Cms/RestaurantMenu.cshtml:11 SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:10 SalesMount/Views/Cms/RestaurantOrders.cshtml:11 SalesMount/Views/Cms/RestaurantStat.cshtml:16 SalesMount/Views/Cms/RestaurantTags.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantDetails.cshtml:14 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenu.cshtml:13 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantTags.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantDetails.cshtml:14
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenu.cshtml:13 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantTags.cshtml:10 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantDetails.cshtml:14 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenu.cshtml:13 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:10 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:11 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:16 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantTags.cshtml:10
msgid "Orders"
msgstr "訂單"

#: SalesMount/Views/Cms/Restaurant.cshtml:30 SalesMount/Views/Cms/RestaurantDetails.cshtml:15 SalesMount/Views/Cms/RestaurantMenu.cshtml:12 SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:11 SalesMount/Views/Cms/RestaurantOrders.cshtml:12 SalesMount/Views/Cms/RestaurantStat.cshtml:17 SalesMount/Views/Cms/RestaurantTags.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantDetails.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenu.cshtml:14 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:17 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantTags.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantDetails.cshtml:15
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenu.cshtml:14 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:17 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantTags.cshtml:11 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantDetails.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenu.cshtml:14 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:11 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:17 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantTags.cshtml:11
msgid "Tags"
msgstr "標籤"

#: SalesMount/Views/Cms/Restaurant.cshtml:31 SalesMount/Views/Cms/RestaurantDetails.cshtml:16 SalesMount/Views/Cms/RestaurantMenu.cshtml:13 SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:12 SalesMount/Views/Cms/RestaurantOrders.cshtml:13 SalesMount/Views/Cms/RestaurantStat.cshtml:18 SalesMount/Views/Cms/RestaurantTags.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantDetails.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenu.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:13 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:18 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantTags.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantDetails.cshtml:16
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenu.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:12 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:13 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:18 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantTags.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantDetails.cshtml:16 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenu.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:12 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:13 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:18 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantTags.cshtml:12
msgid "Statistics"
msgstr "統計"

#: SalesMount/Views/Cms/Restaurant.cshtml:86
msgid "Image"
msgstr "圖片"

#: SalesMount/Views/Cms/RestaurantDetails.cshtml:22
msgid "Add"
msgstr "增加"

#: SalesMount/Views/Cms/RestaurantDetails.cshtml:32
msgid "Edit"
msgstr "編輯"

#: SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:23 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:23
msgid "Menu Name"
msgstr "菜單名稱"

#: SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:29 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:29 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:29 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:29
msgid "Menu Image"
msgstr "菜單圖片"

#: SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:43 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:43 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:43 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:43
msgid "Food Name"
msgstr "菜名"

#: SalesMount/Views/Cms/RestaurantMenuEdit.cshtml:49 SalesMount/Views/Cms/RestaurantOrders.cshtml:20 SalesMount/Views/Cms/RestaurantOrders.cshtml:38 SalesMount/Views/Restaurant/Order.cshtml:15 SalesMount/Views/Users/Orders.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantMenuEdit.cshtml:49 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:20 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Order.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Orders.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantMenuEdit.cshtml:49 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:20 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:38
#: SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Order.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Orders.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantMenuEdit.cshtml:49 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:20 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:38 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Order.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Orders.cshtml:15
msgid "Price"
msgstr "價錢"

#: SalesMount/Views/Cms/RestaurantOrders.cshtml:21 SalesMount/Views/Cms/RestaurantOrders.cshtml:39 SalesMount/Views/Restaurant/Order.cshtml:16 SalesMount/Views/Users/Orders.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantOrders.cshtml:39 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Order.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Orders.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantOrders.cshtml:39 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Order.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Orders.cshtml:16 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:21
#: SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantOrders.cshtml:39 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Order.cshtml:16 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Orders.cshtml:16
msgid "Quantity"
msgstr "數量"

#: SalesMount/Views/Cms/RestaurantStat.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:22 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:22
msgid "Income"
msgstr "收入"

#: SalesMount/Views/Cms/RestaurantStat.cshtml:80 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:80 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:80 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:80
msgid "Length of stay"
msgstr "逗留時間"

#: SalesMount/Views/Cms/RestaurantStat.cshtml:123 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Cms/RestaurantStat.cshtml:123 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Cms/RestaurantStat.cshtml:123 SalesMount/obj/Release/Package/PackageTmp/Views/Cms/RestaurantStat.cshtml:123
msgid "Gender"
msgstr "性別"

#: SalesMount/Views/Home/Index.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:21 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:21
msgid "At rush hour of restaurant"
msgstr "向繁忙時間"

#: SalesMount/Views/Home/Index.cshtml:27 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:27 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:27 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:27
msgid "Usually, <span class=\"redStrong\"> no staff </span> to service you?"
msgstr "<span class=\"redStrong\">冇</span>侍應為您服務？"

#: SalesMount/Views/Home/Index.cshtml:36 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:36 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:36 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:36
msgid "<span class=\"redStrong\">Tap </span> SalesMount NFC tag"
msgstr "<span class=\"redStrong\">拍</span>一下 SalesMount NFC 標籤"

#: SalesMount/Views/Home/Index.cshtml:40 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:40 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:40 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:40
msgid "To"
msgstr "然後"

#: SalesMount/Views/Home/Index.cshtml:45 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:45 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:45 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:45
msgid "<span class=\"redStrong\">Get </span>menu and <span class=\"redStrong\">Order </span>food"
msgstr "<span class=\"redStrong\">取得</span>菜單 然後 <span class=\"redStrong\"> 落單</span>"

#: SalesMount/Views/Home/Index.cshtml:69 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:69 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:69 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:69
msgid "Wait"
msgstr "等"

#: SalesMount/Views/Home/Index.cshtml:74 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:74 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:74 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:74
msgid "A While"
msgstr "一陣"

#: SalesMount/Views/Home/Index.cshtml:90 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:90 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:90 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:90
msgid "Time to dining!"
msgstr "開餐啦！"

#: SalesMount/Views/Home/Index.cshtml:96 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Home/Index.cshtml:96 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Home/Index.cshtml:96 SalesMount/obj/Release/Package/PackageTmp/Views/Home/Index.cshtml:96
msgid "Get Apps Now!"
msgstr "馬上安裝"

#: SalesMount/Views/Obj/Index.cshtml:2 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Obj/Index.cshtml:2 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Obj/Index.cshtml:2 SalesMount/obj/Release/Package/PackageTmp/Views/Obj/Index.cshtml:2
msgid "Unrecognized tag"
msgstr "無法識別的標籤"

#: SalesMount/Views/Obj/Index.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Obj/Index.cshtml:15 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Obj/Index.cshtml:15 SalesMount/obj/Release/Package/PackageTmp/Views/Obj/Index.cshtml:15
msgid "Unrecognized tag!"
msgstr "無法識別的標籤!"

#: SalesMount/Views/Restaurant/Index.cshtml:52
msgid "Checkout"
msgstr "落單"

#: SalesMount/Views/Restaurant/Index.cshtml:53 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Index.cshtml:42 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Index.cshtml:42 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Index.cshtml:42
msgid "Amount: "
msgstr "金額:"

#: SalesMount/Views/Restaurant/Index.cshtml:63 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Index.cshtml:52 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Index.cshtml:52 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Index.cshtml:52
msgid "OK"
msgstr "OK"

#: SalesMount/Views/Restaurant/Index.cshtml:64 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Index.cshtml:53 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Index.cshtml:53 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Index.cshtml:53
msgid "Cancel"
msgstr "取消"

#: SalesMount/Views/Restaurant/Order.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Order.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Order.cshtml:9 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Order.cshtml:9
msgid "Order is processing..."
msgstr "正在處理你訂單..."

#: SalesMount/Views/Restaurant/Order.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Order.cshtml:10 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Order.cshtml:10 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Order.cshtml:10
msgid "Please wait a moment."
msgstr "請稍等片刻"

#: SalesMount/Views/Shared/_EmailLayout.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_EmailLayout.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_EmailLayout.cshtml:23 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_EmailLayout.cshtml:23
msgid "Please do NOT REPLY. This letter is generated by computer system."
msgstr "請不要回复。這封信是由電腦系統生成的。"

#: SalesMount/Views/Shared/_LayoutCms.cshtml:22 SalesMount/Views/Shared/_LayoutSite.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutCms.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:26 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutCms.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:26 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutCms.cshtml:22 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:26
#, csharp-format
msgid "Hi, {0}"
msgstr "Hi, {0}"

#: SalesMount/Views/Shared/_LayoutCms.cshtml:23 SalesMount/Views/Shared/_LayoutSite.cshtml:28 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutCms.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:28 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutCms.cshtml:23 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:28 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutCms.cshtml:23 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:28
msgid "Logout"
msgstr "登出"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:21 SalesMount/Views/Shared/_LayoutSite.cshtml:40 SalesMount/Views/Users/Signup.cshtml:11 SalesMount/Views/Users/Signup.cshtml:29 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:40 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:16 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:21 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:40 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:11 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:16 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:21
#: SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:40 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:11 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:16
msgid "Sign up"
msgstr "註冊"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:22 SalesMount/Views/Users/Login.cshtml:3 SalesMount/Views/Users/Login.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Login.cshtml:3 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Login.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:22 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Login.cshtml:3 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Login.cshtml:38 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:22 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Login.cshtml:3 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Login.cshtml:38
msgid "Login"
msgstr "登入"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:27 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:27 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:27 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:27
msgid "Restaurant CMS"
msgstr "餐廳CMS"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:38 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:38
msgid "Navigation"
msgstr "瀏覽"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:41 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:41 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:41 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:41
msgid "About us"
msgstr "關於我們"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:42 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:42 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:42 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:42
msgid "Support"
msgstr "取得支援"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:44 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:44 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:44 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:44
msgid "Policies and Terms"
msgstr "政策和條款"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:46 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:46 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:46 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:46
msgid "Privacy & cookies"
msgstr "隱私和 cookies"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:47 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:47 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:47 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:47
msgid "Terms of use"
msgstr "使用條款"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:48 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:48 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:48 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:48
msgid "Trademarks"
msgstr "商標"

#: SalesMount/Views/Shared/_LayoutSite.cshtml:50 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Shared/_LayoutSite.cshtml:50 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Shared/_LayoutSite.cshtml:50 SalesMount/obj/Release/Package/PackageTmp/Views/Shared/_LayoutSite.cshtml:50
msgid "Languages"
msgstr "語言"

#: SalesMount/Views/Users/Activation.cshtml:3 SalesMount/Views/Users/Activation.cshtml:19 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:3 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:19 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:3 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:19 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:3 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:19
msgid "Activation"
msgstr "啟動"

#: SalesMount/Views/Users/Activation.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:38 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:38 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:38
msgid "Active"
msgstr "啟動"

#: SalesMount/Views/Users/Activation.cshtml:45 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:45 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:45 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:45
msgid "Activation Success"
msgstr "啟動成功"

#: SalesMount/Views/Users/Activation.cshtml:46 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Activation.cshtml:46 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Activation.cshtml:46 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Activation.cshtml:46
msgid "Account was actived"
msgstr "賬戶被啟動"

#: SalesMount/Views/Users/Orders.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Orders.cshtml:9 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Orders.cshtml:9 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Orders.cshtml:9
msgid "Orders History"
msgstr "訂單記錄"

#: SalesMount/Views/Users/Signup.cshtml:17
msgid "Male"
msgstr "男 "

#: SalesMount/Views/Users/Signup.cshtml:22
msgid "Female"
msgstr "女"

#: SalesMount/Views/Users/Signup.cshtml:63
msgid "YYYY"
msgstr "YYYY"

#: SalesMount/Views/Users/Signup.cshtml:64
msgid "MM"
msgstr "MM"

#: SalesMount/Views/Users/Signup.cshtml:65
msgid "DD"
msgstr "DD"

#: SalesMount/Views/Users/Signup.cshtml:97 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:66 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:66 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:66
msgid "Confirm your password"
msgstr "確認密碼"

#: SalesMount/Views/Users/Signup.cshtml:104 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:73 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:73 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:73
#, csharp-format
msgid "I agree SalesMount <a href=\"{0}\">Terms of use</a> and <a href=\"{1}\">Privacy & cookies policy</a>"
msgstr "我同意 SalesMount <a href=\"{0}\">使用條款</a>及<a href=\"{1}\">隱私及cookies政策</a>"

#: SalesMount/Views/Users/Signup.cshtml:105 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/Signup.cshtml:74 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/Signup.cshtml:74 SalesMount/obj/Release/Package/PackageTmp/Views/Users/Signup.cshtml:74
msgid "Agree and Sign up"
msgstr "同意並註冊"

#: SalesMount/Views/Users/SignupSuccess.cshtml:4 SalesMount/Views/Users/SignupSuccess.cshtml:18 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/SignupSuccess.cshtml:4 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/SignupSuccess.cshtml:18 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/SignupSuccess.cshtml:4 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/SignupSuccess.cshtml:18 SalesMount/obj/Release/Package/PackageTmp/Views/Users/SignupSuccess.cshtml:4 SalesMount/obj/Release/Package/PackageTmp/Views/Users/SignupSuccess.cshtml:18
msgid "Sign up succeed!"
msgstr "註冊成功！"

#: SalesMount/Views/Users/SignupSuccess.cshtml:19 SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/SignupSuccess.cshtml:19 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/SignupSuccess.cshtml:19 SalesMount/obj/Release/Package/PackageTmp/Views/Users/SignupSuccess.cshtml:19
msgid "Thank you!"
msgstr "謝謝！"

#: SalesMount/Views/Users/SignupSuccess.cshtml:20
#, csharp-format
msgid "The activation code was sent to {0}. After received the code, please go to activation page <a href=\"{1}\">{1}</a>."
msgstr "激活碼發送到{0}。收到代碼後，請前往激活頁面<a href=\"{1}\">{1}</A>。"

#: SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Restaurant/Index.cshtml:41 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Restaurant/Index.cshtml:41 SalesMount/obj/Release/Package/PackageTmp/Views/Restaurant/Index.cshtml:41
msgid "Check out"
msgstr ""

#: SalesMount/obj/Release/AspnetCompileMerge/Source/Views/Users/SignupSuccess.cshtml:20 SalesMount/obj/Release/AspnetCompileMerge/TempBuildDir/Views/Users/SignupSuccess.cshtml:20 SalesMount/obj/Release/Package/PackageTmp/Views/Users/SignupSuccess.cshtml:20
#, csharp-format
msgid "The Activation mail was sent to {0}. Please check your mailbox."
msgstr "啟動碼送到 {0}。請檢查您的郵箱。"

#~ msgid "E-mail"
#~ msgstr "E-mail"
