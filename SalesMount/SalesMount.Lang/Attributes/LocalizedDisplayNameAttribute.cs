﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Lang.Attributes
{
	public class LocalizedDisplayNameAttribute : DisplayNameAttribute
	{
		private readonly string message;
		public LocalizedDisplayNameAttribute(string message)
			:base()
		{
			this.message = message;
		}

		public override string DisplayName
		{
			get
			{
				return GetText.T(message);
			}
		}
	}
}
