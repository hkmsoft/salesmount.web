﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SalesMount.Web.Helpers
{
	public static class CUrlHelper
	{
		
		public static string GetAction(Expression member)
		{
			var memberEx = member as MethodCallExpression;

			if (memberEx == null)
				throw new ArgumentException("Body not a member-expression.");

			string name = memberEx.Method.Name;

			return name;
		}

		public static string GetActionUrl<T1>(this UrlHelper helper, Expression<Func<T1, ActionResult>> member)
		{
			var memberEx = member.Body as MethodCallExpression;

			if (memberEx == null)
				throw new ArgumentException("Body not a member-expression.");

			string rawCtrlName = typeof(T1).Name;
			string ctrlName = rawCtrlName.Remove(rawCtrlName.LastIndexOf("Controller"));

			var paramsOj = memberEx.Method.GetParameters();
			RouteValueDictionary urlParams = new RouteValueDictionary();
			for (int i = 0; i < paramsOj.Length; i++)
			{
				var paramInfo = paramsOj[i];
				LambdaExpression l = Expression.Lambda(memberEx.Arguments[i]);
				var compiledExp = l.Compile();
				var value = compiledExp.DynamicInvoke();

				if (value != null)
				{
					urlParams.Add(paramInfo.Name, value);
				}
			}


			return helper.Action(memberEx.Method.Name, ctrlName, urlParams);
		}

		public static string CurruntUrl(this UrlHelper helper, object changeVal)
		{
			return CurruntUrl(helper, changeVal, null);
		}

		public static string CurruntUrl(this UrlHelper helper, object changeVal, string[] valRemove)
		{
			var OrgData = helper.RequestContext.RouteData.Values;
			RouteValueDictionary newData;
			newData = new RouteValueDictionary(changeVal);

			foreach (var item in OrgData)
			{
				if (!newData.ContainsKey(item.Key))
				{
					if (valRemove != null)
					{
						if (!valRemove.Contains(item.Key))
						{
							newData.Add(item.Key, item.Value);
						}
					}
					else
					{
						newData.Add(item.Key, item.Value);
					}
				}
			}

			var queryStr = helper.RequestContext.HttpContext.Request.QueryString;
			for (int i = 0; i < queryStr.Count; i++)
			{
				newData.Add(queryStr.Keys[i], queryStr[i]);
			}

			return helper.RouteUrl(newData);
		}

	}
}
