﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SalesMount.Web.Helpers
{
	public enum ScriptPosition { HEAD, FOOT };
	public enum ScriptPriority { HIGH = 0, NORMAL = 1, LOW = 2 };
	public static class ScriptHelper
	{

		public class ScriptObject
		{
			public string Url { get; set; }
		}

		private class ClientScriptsHandler
		{
			private Dictionary<string, object> scriptConfig;

			private int[] jsFilesPriorityIndex = { 0, 0x10000, 0x20000 };
			private int[] cssFilesPriorityIndex = { 0, 0x10000, 0x20000 };
			private Dictionary<ScriptPosition, SortedList<int, ScriptObject>> scriptFilesLists = new Dictionary<ScriptPosition, SortedList<int, ScriptObject>>();
			private SortedList<int, ScriptObject> cssFilesList = new SortedList<int, ScriptObject>();
			private List<int> scriptsSet = new List<int>();

			public void AddCss(HtmlHelper helper, string scriptUrl, ScriptPriority priority)
			{
				int sUrlHash = scriptUrl.GetHashCode();
				if (!scriptsSet.Contains(sUrlHash))
				{
					var script = new ScriptObject();
					script.Url = scriptUrl;
					cssFilesList.Add(cssFilesPriorityIndex[(int)priority]++, script);
				}
			}

			public void AddScript(HtmlHelper helper, string scriptUrl, ScriptPriority priority, ScriptPosition position)
			{
				int sUrlHash = scriptUrl.GetHashCode();
				if (!scriptsSet.Contains(sUrlHash))
				{
					scriptsSet.Add(sUrlHash);
					if (!scriptFilesLists.ContainsKey(position))
					{
						scriptFilesLists.Add(position, new SortedList<int, ScriptObject>());
					}

					var script = new ScriptObject();
					script.Url = scriptUrl;
					scriptFilesLists[position].Add(jsFilesPriorityIndex[(int)priority]++, script);
				}
			}

			public void PassJsConfig(HtmlHelper helper, string name, object value)
			{
				if (scriptConfig == null)
					scriptConfig = new Dictionary<string, object>();

				scriptConfig[name] = value;
			}

			public void LoadLang(HtmlHelper helper)
			{
				var langName = System.Threading.Thread.CurrentThread.CurrentUICulture.Name.ToLower();
				var langJs = UrlHelper.GenerateContentUrl("~/Content/js/DW.lang." + langName + ".js", helper.ViewContext.HttpContext);
				AddScript(helper, langJs, ScriptPriority.HIGH, ScriptPosition.HEAD);
			}

			public HtmlString FlushScripts(HtmlHelper helper, ScriptPosition position = ScriptPosition.HEAD)
			{
				StringBuilder scriptTagsHtml = new StringBuilder();

				if (position == ScriptPosition.HEAD)
				{
					foreach (var script in cssFilesList)
					{
						var scriptTag = new TagBuilder("link");
						scriptTag.Attributes.Add("rel", "StyleSheet");
						scriptTag.Attributes.Add("type", "text/css");
						scriptTag.Attributes.Add("href", script.Value.Url);
						scriptTagsHtml.Append(scriptTag.ToString(TagRenderMode.SelfClosing));
					}

					if (scriptConfig != null)
					{
						System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
						var scriptTag = new TagBuilder("script");
						scriptTag.Attributes.Add("type", "text/javascript");
						scriptTag.InnerHtml = "window.SalesMount || (SalesMount = {});window.SalesMount.config=" + oSerializer.Serialize(scriptConfig) + ";";
						scriptTagsHtml.Append(scriptTag.ToString());
					}
				}

				if (scriptFilesLists.ContainsKey(position))
				{
					var sList = scriptFilesLists[position];
					scriptTagsHtml.Append("<!-- Total script file :" + sList.Count + "-->");
					foreach (var script in sList)
					{
						var scriptTag = new TagBuilder("script");
						scriptTag.Attributes.Add("src", script.Value.Url);
						scriptTag.Attributes.Add("type", "text/javascript");
						scriptTagsHtml.Append(scriptTag.ToString());
					}
				}

				return new HtmlString(scriptTagsHtml.ToString());
			}
		}


		private static ClientScriptsHandler GetCurrentScriptsHandler(HtmlHelper helper)
		{
			ClientScriptsHandler scriptsHandler;
			if (helper.ViewContext.TempData.ContainsKey("ClientScriptHelper"))
			{
				scriptsHandler = helper.ViewContext.TempData["ClientScriptHelper"] as ClientScriptsHandler;
			}
			else
			{
				scriptsHandler = new ClientScriptsHandler();
				helper.ViewContext.TempData.Add("ClientScriptHelper", scriptsHandler);
			}
			return scriptsHandler;
		}

		public static void AddCss(this HtmlHelper helper, string scriptPath)
		{
			AddCss(helper, scriptPath, ScriptPriority.NORMAL);
		}

		public static void AddCss(this HtmlHelper helper, string scriptPath, ScriptPriority priority)
		{
			GetCurrentScriptsHandler(helper).AddCss(helper, scriptPath, priority);
		}

		public static void AddContentCss(this HtmlHelper helper, string scriptPath)
		{
			AddContentCss(helper, scriptPath, ScriptPriority.NORMAL);
		}

		public static void AddContentCss(this HtmlHelper helper, string scriptPath, ScriptPriority priority)
		{
			GetCurrentScriptsHandler(helper).AddCss(helper, UrlHelper.GenerateContentUrl(scriptPath, helper.ViewContext.HttpContext), priority);
		}

		public static void AddContentScript(this HtmlHelper helper, string scriptUrl)
		{
			AddContentScript(helper, scriptUrl, ScriptPriority.NORMAL, ScriptPosition.HEAD);
		}

		public static void AddContentScript(this HtmlHelper helper, string scriptUrl, ScriptPosition position)
		{
			AddContentScript(helper, scriptUrl, ScriptPriority.NORMAL, position);
		}

		public static void AddContentScript(this HtmlHelper helper, string scriptUrl, ScriptPriority priority)
		{
			AddContentScript(helper, scriptUrl, priority, ScriptPosition.HEAD);
		}

		public static void AddContentScript(this HtmlHelper helper, string scriptUrl, ScriptPriority priority, ScriptPosition position)
		{
			AddScript(helper, UrlHelper.GenerateContentUrl(scriptUrl, helper.ViewContext.HttpContext), priority, position);
		}

		public static void AddScript(this HtmlHelper helper, string scriptUrl, ScriptPriority priority = ScriptPriority.NORMAL, ScriptPosition position = ScriptPosition.HEAD)
		{
			GetCurrentScriptsHandler(helper).AddScript(helper, scriptUrl, priority, position);
		}

		public static void PassJsConfig(this HtmlHelper helper, string name, object value)
		{
			GetCurrentScriptsHandler(helper).PassJsConfig(helper, name, value);
		}

		public static void LoadLang(this HtmlHelper helper)
		{
			GetCurrentScriptsHandler(helper).LoadLang(helper);
		}

		public static HtmlString FlushScripts(this HtmlHelper helper, ScriptPosition position = ScriptPosition.HEAD)
		{
			return GetCurrentScriptsHandler(helper).FlushScripts(helper, position);
		}
	}
}
