﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SalesMount.Web.Helpers
{
	public static class AppHelper
	{
		public static bool IsApp()
		{
			String userAgent = System.Web.HttpContext.Current.Request.UserAgent;
			return userAgent != null && userAgent.Contains("SalesMount/");
		}
	}
}
