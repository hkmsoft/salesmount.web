﻿using SalesMount.Lang;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace SalesMount.Web.Helpers
{
	public static class LanguageHelper
	{
		public class LanguageLinkInfo
		{
			public string Url { get; set; }
			public string Text { get; set; }
			public string ActionName { get; set; }
			public string ControllerName { get; set; }
			public RouteValueDictionary RouteValues { get; set; }
			public bool IsSelected { get; set; }
			public string ClassName { get; set; }

			public MvcHtmlString HtmlSafeUrl
			{
				get
				{
					return MvcHtmlString.Create(Url);
				}
			}
		}

		//private static RouteValueDictionary storedRouteValues = null;

		public static LanguageLinkInfo LanguageUrl(HtmlHelper helper, string cultureName, string text, RouteValueDictionary routeValues,
			string languageRouteName = "Localization", bool strictSelected = false)
		{
			// set the input language to lower
			cultureName = cultureName.ToLower();

			// set the language into route values
			routeValues["lang"] = cultureName;

			var actionName = routeValues["action"].ToString();
			var controllerName = routeValues["controller"].ToString();

			// generate the language specify url
			var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
			var url = urlHelper.RouteUrl(languageRouteName, routeValues);

			// check whether the current thread ui culture is this language
			var current_lang_name = Thread.CurrentThread.CurrentUICulture.Name.ToLower();
			var isSelected = strictSelected ?
				current_lang_name == cultureName :
			current_lang_name.StartsWith(cultureName);
			return new LanguageLinkInfo()
			{
				Url = url,
				Text = text,
				ActionName = actionName,
				ControllerName = controllerName,
				RouteValues = routeValues,
				IsSelected = isSelected
			};
		}

		private static RouteValueDictionary GetRouteData(HtmlHelper helper)
		{
			RouteValueDictionary routeValues;
			// retrieve the route values from the view context
			routeValues = new RouteValueDictionary(helper.ViewContext.RouteData.Values);
			// copy the query strings into the route values to generate the link
			var queryString = helper.ViewContext.HttpContext.Request.QueryString;
			foreach (string key in queryString)
			{
				if (queryString[key] != null && !string.IsNullOrWhiteSpace(key))
				{
					if (routeValues.ContainsKey(key))
					{
						routeValues[key] = queryString[key];
					}
					else
					{
						routeValues.Add(key, queryString[key]);
					}
				}
			}

			return routeValues;
		}

		public static HelperResult LanguageSelectors(this HtmlHelper helper, IDictionary<string, string> cultureNames,
			string selectedCssClass, Func<LanguageLinkInfo, HelperResult> template, string languageRouteName = "Localization")
		{
			return new HelperResult(writer =>
			{
				RouteValueDictionary routeValues = LanguageHelper.GetRouteData(helper);
				foreach (var cultureName in cultureNames)
				{

					var language = LanguageHelper.LanguageUrl(helper, cultureName.Key, cultureName.Value, routeValues, languageRouteName, false);

					language.ClassName = (language.IsSelected) ? selectedCssClass + " " + cultureName.Key : cultureName.Key;
					template(language).WriteTo(writer);

				}
			});
		}

		public static string T(this HtmlHelper helper, string key)
		{
			return GetText.T(key);
		}
		public static string T(this HtmlHelper helper, string key, params string[] replaces)
		{
			return GetText.T(key, replaces);
		}
		public static string T(this HtmlHelper helper, string key, Dictionary<string, string> replaces)
		{
			return GetText.T(key, replaces);
		}

		public static HtmlString ScriptT(this HtmlHelper helper, string key)
		{
			return new HtmlString(HttpUtility.JavaScriptStringEncode(GetText.T(key)));
		}

		public static HtmlString ScriptT(this HtmlHelper helper, string key, params string[] replaces)
		{
			return new HtmlString(HttpUtility.JavaScriptStringEncode(GetText.T(key, replaces)));
		}

		public static HtmlString ScriptT(this HtmlHelper helper, string key, Dictionary<string, string> replaces)
		{
			return new HtmlString(HttpUtility.JavaScriptStringEncode(GetText.T(key, replaces)));
		}

		public static HtmlString RawT(this HtmlHelper helper, string key)
		{
			return new HtmlString(GetText.T(key));
		}

		public static HtmlString RawT(this HtmlHelper helper, string key, params string[] replaces)
		{
			return new HtmlString(GetText.T(key, replaces));
		}

		public static HtmlString RawT(this HtmlHelper helper, string key, Dictionary<string, string> replaces)
		{
			return new HtmlString(GetText.T(key, replaces));
		}

	}
}
